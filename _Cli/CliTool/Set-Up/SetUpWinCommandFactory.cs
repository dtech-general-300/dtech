﻿using CliTool._helpers;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace CliTool.Set_Up
{
    public class SetUpWinCommandFactory : CommandFactory
    {
        private const string defaultProfilePath = "$Profile";

        public override Command CreateCommand()
        {
            var command = new Command("setup-win", "Instala para Windows todo lo necesario para dtech command (suggestions, profile, policies, templates)")
            {
                new Argument<FileInfo>(
                    name: "profile-path",
                    getDefaultValue: ()=>new FileInfo(defaultProfilePath),
                    description: "Indica la direccion del archivo profile de PowerShell(.ps1)"),
                new Option<bool>(
                    new[] { "--reset-templates", "-r" },
                    description: "Elimina todos los custom-templates instalados, lo hace durante la instalacion")
            };

            command.Handler = CommandHandler.Create<FileInfo, bool>(Handler);
            return command;
        }

        public static async Task Handler(FileInfo profilePath, bool resetTemplates)
        {
            try
            {
                if (!HProcess.IsTheCorrectOS(OSPlatform.Windows)) return;
                if (!H_WinHelper.IsAdministratorUser()) return; // cause we need admin rigths to change Set-ExecutionPolicy

                var profileDir = await GetProfileDirectory(profilePath);

                await InstallSuggestions();
                await UpdateProfile(profileDir);
                await CheckExecutionPolicy();
                await InstallCustomTemplates(resetTemplates);

                HConsole.Custom(">Setup finished!\n", ConsoleColor.DarkGreen);
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }


        #region SRP Methods
        public static async Task<string> GetProfileDirectory(FileInfo profilePath)
        {
            var profileDir = profilePath.FullName;
            if (profilePath.Name == defaultProfilePath) { profileDir = await H_WinHelper.GetProfileDir(); }
            return profileDir;
        }

        public static async Task InstallSuggestions()
        {
            HConsole.Title("Installing dotnet-suggest");
            await HProcess.RunProcess("dotnet", "tool install --global dotnet-suggest");
        }

        public static async Task UpdateProfile(string profilePath)
        {
            HConsole.Title("Updating Profile");
            HConsole.Normal($"Profile Path: {profilePath}");

            HConsole.SubTitle("dotnet-suggest script");
            var script = await HSetupHelper.GetScript("dotnet-suggest-shim", linux: false);
            await HSetupHelper.UpsertProfileWithScript(profilePath, script);

            HConsole.SubTitle("reset-templates script");
            var resetScript = await HSetupHelper.GetScript("dotnet-reset-templates", linux: false);
            await HSetupHelper.UpsertProfileWithScript(profilePath, resetScript);

            Console.WriteLine();
        }

        public static async Task CheckExecutionPolicy()
        {
            HConsole.Title("Check Execution-Policy");
            var currentPolicy = await H_WinHelper.GetExecutionPolicy();
            if (currentPolicy != "RemoteSigned")
            {
                var res = "";
                while (res != "S" && res != "N")
                {
                    HConsole.Normal("Desea actualizar la politica a RemoteSigned? Si[S] No[N]: ");
                    res = Console.ReadLine();
                }
                if (res == "N")
                {
                    HConsole.Normal("\nLa política NO ha cambiado, es posible que PowerShell no ejecute el profile.");
                    HConsole.Normal("Si desea cambiar la política después ejecute:");
                    HConsole.Normal("Set-ExecutionPolicy RemoteSigned -Scope LocalMachine");
                    HConsole.Normal("Presione ENTER para continuar con el resto de la instalación...");
                    Console.ReadKey();
                    return;
                }
                await H_WinHelper.SetExecutionPolicy("RemoteSigned");
            }
            HConsole.Normal($"Política actual: {await H_WinHelper.GetExecutionPolicy()}");
            Console.WriteLine();
        }

        public static async Task InstallCustomTemplates(bool resetTemplates)
        {
            HConsole.Title("Install custom templates");

            if (resetTemplates)
            {
                HConsole.SubTitle("Reset-Templates");
                await HProcess.RunProcess("powershell", "Reset-Templates");
            }

            HConsole.SubTitle("Installing Templates");
            var templatesPath = HSetupHelper.GetTemplatesPath();
            var result = await HTemplates.InstallTemplates(templatesPath);

            HConsole.SubTitle("CheckIfInstalled");
            await HSetupHelper.CheckInstallation(templatesPath, result);

            Console.WriteLine();
        }
        #endregion
    }
}
