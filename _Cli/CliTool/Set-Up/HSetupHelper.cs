﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CliTool.Set_Up
{
    public static class HSetupHelper
    {
        #region Scripts
        public static async Task<string[]> GetScript(string filename, bool linux)
        {
            string shimPath = Path.Combine(
                HDirectory.RootDirectory,
                HDirectory.CoreCodeFolder,
                "_Cli", "CliTool", "Set-Up",
                $"{filename}." + (linux ? "bash" : "ps1"));
            return await File.ReadAllLinesAsync(shimPath);
        }

        public static async Task UpsertProfileWithScript(string profilePath, string[] script)
        {
            if (File.Exists(profilePath))
            {
                HConsole.Normal("File founded, updating...");

                // only update if not previously updated
                foreach (var line in await File.ReadAllLinesAsync(profilePath))
                    if (line == script[0])
                    {
                        HConsole.Normal("Already Updated!");
                        return;
                    }

                // update
                using var sw = new StreamWriter(profilePath, true);
                sw.WriteLine("");
                foreach (var line in script)
                    sw.WriteLine(line);
                HConsole.Normal("Updated!");
            }
            else
            {
                HConsole.Normal("File not founded, creating...");
                var FoldersPath = new DirectoryInfo(profilePath).Parent.FullName; // get path without filename
                Directory.CreateDirectory(FoldersPath); // create all non existing folders in a path
                await File.WriteAllLinesAsync(profilePath, script);
                HConsole.Normal("Created!");
            }
        }
        #endregion


        #region Templates
        public static string GetTemplatesPath() => Path.Combine(
            HDirectory.RootDirectory,
            HDirectory.CoreCodeFolder,
            "_Templates");

        public static async Task CheckInstallation(string templatesPath, List<string> result)
        {
            var counter = 0;
            var jsons = await GetJsonTemplates(templatesPath);
            foreach (var json in jsons)
                foreach (var line in result)
                    if (!string.IsNullOrEmpty(line) && line.Contains(json.Name) && line.Contains(json.ShortName))
                    {
                        HConsole.Normal(line);
                        counter++;
                    }
            if (jsons.Count == counter)
                HConsole.Normal("Installation Ok!");
            else
                HConsole.Error("Not completed!");
        }

        private static async Task<List<MTemplateModel>> GetJsonTemplates(string templatesPath)
        {
            var jsons = new List<MTemplateModel>();
            foreach (var posibleFolder in Directory.GetDirectories(templatesPath))
                foreach (var subFolder in Directory.GetDirectories(posibleFolder))
                    if (subFolder.Contains(".template.config"))
                    {
                        var path = Path.Combine(subFolder, "template.json");
                        var text = await File.ReadAllTextAsync(path);
                        jsons.Add(JsonSerializer.Deserialize<MTemplateModel>(text));
                    }

            if (jsons.Count == 0)
                throw new FileNotFoundException("No se encontro ningun template válido (*/.template.config/template.json)");

            return jsons;
        }
        #endregion
    }
}
