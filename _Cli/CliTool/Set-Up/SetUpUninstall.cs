﻿using CliTool._helpers;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace CliTool.Set_Up
{
    public class SetUpUninstall : CommandFactory
    {
        public override Command CreateCommand()
        {
            var command = new Command("setup-uninstall", "Desintala la herramienta de manera global")
            {
            };

            command.Handler = CommandHandler.Create(Handler);
            return command;
        }

        public static void Handler()
        {
            try
            {
                if (!AskForConfirmation()) return;
                if (HProcess.IsTheCorrectOS(OSPlatform.Linux, false)) UninstallToolLinux();
                if (HProcess.IsTheCorrectOS(OSPlatform.Windows, false)) UninstallToolWindows();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }


        #region SRP Methods
        public static bool AskForConfirmation()
        {
            HConsole.Title("Confimation");
            var res = "";
            while (res != "S" && res != "N")
            {
                HConsole.Normal("Seguro que desea desinstalar? Si[S] No[N]: ");
                res = Console.ReadLine();
            }
            if (res == "N")
            {
                HConsole.Warn("\nNo se desinstaló ni se eliminó ningun componente.");
                return false;
            }
            Console.WriteLine();
            return true;
        }


        public static void UninstallToolLinux()
        {
            HConsole.Error("Not supported yet!");
        }

        public static void UninstallToolWindows()
        {
            HConsole.Title($"Uninstalling tool");
            var title = "Write-Host \"Preparing for uninstall...`n`n\";";
            var delay = "Start-Sleep -Milliseconds 1400;";
            var separator = "Write-Host \"`n`n\";";
            HProcess.RunProcessInNewWindow("powershell",
                title +
                delay +
                "dotnet tool uninstall -g clitool;" +
                separator +
                "timeout /t -1", HDirectory.RootDirectory);
            HConsole.Warn("Process opened in other window.");
        }
        #endregion
    }
}
