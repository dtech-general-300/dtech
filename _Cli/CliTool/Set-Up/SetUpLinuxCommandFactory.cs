﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Set_Up
{
    public class SetUpLinuxCommandFactory : CommandFactory
    {
        private const string defaultProfilePath = "~/.bash_profile";

        public override Command CreateCommand()
        {
            var command = new Command("setup-linux", "Instala para Linux todo lo necesario para dtech command (suggestions, profile, templates)")
            {
                new Argument<FileInfo>(
                    name: "profile-path",
                    getDefaultValue: () => new FileInfo(defaultProfilePath),
                    description: "Indica la direccion del archivo profile (esto habilita tab-completion)"),
                new Option<bool>(
                    new[] { "--reset-templates", "-r" },
                    description: "Elimina todos los custom-templates instalados, lo hace durante la instalacion")
            };
            command.Handler = CommandHandler.Create<FileInfo, bool>(Handler);
            return command;
        }

        public static async Task Handler(FileInfo profilePath, bool resetTemplates)
        {
            try
            {
                if (!HProcess.IsTheCorrectOS(OSPlatform.Linux)) return;
                
                var profileDir = GetProfileDirectory(profilePath);

                await InstallSuggestions();
                await UpdateProfile(profileDir);
                await InstallCustomTemplates(resetTemplates);

                HConsole.Custom(">Setup finished!\n", ConsoleColor.DarkGreen);
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }

        #region SRP Methods
        public static string GetProfileDirectory(FileInfo profilePath)
        {
            var profileDir = profilePath.FullName;
            if (profilePath.Name == defaultProfilePath) { profileDir = defaultProfilePath; }
            return profileDir;
        }

        public static async Task InstallSuggestions()
        {
            HConsole.Title("Installing dotnet-suggest");
            await HProcess.RunProcess("dotnet", "tool install --global dotnet-suggest");
        }

        public static async Task UpdateProfile(string profilePath)
        {
            HConsole.Title("Updating Profile");
            HConsole.Normal($"Profile Path: {profilePath}");

            HConsole.SubTitle("dotnet-suggest script");
            var script = await HSetupHelper.GetScript("dotnet-suggest-shim", linux: true);
            await HSetupHelper.UpsertProfileWithScript(profilePath, script);
            Console.WriteLine();
        }

        public static async Task InstallCustomTemplates(bool resetTemplates)
        {
            HConsole.Title("Install custom templates");

            if (resetTemplates)
            {
                HConsole.SubTitle("Reset-Templates");
                HConsole.Error("Not supported yet.");
                // TODO: support for reset-templates in linux (maybe is just clean the .templateengine folder)
            }

            HConsole.SubTitle("Installing Templates");
            var templatesPath = HSetupHelper.GetTemplatesPath();
            var result = await HTemplates.InstallTemplates(templatesPath);

            HConsole.SubTitle("CheckIfInstalled");
            await HSetupHelper.CheckInstallation(templatesPath, result);

            Console.WriteLine();
        }
        #endregion
    }
}
