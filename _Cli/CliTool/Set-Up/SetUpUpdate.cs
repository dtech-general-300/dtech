﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Runtime.InteropServices;

namespace CliTool.Set_Up
{
    public class SetUpUpdate : CommandFactory
    {
        private const string defaultVersion = "None";

        public override Command CreateCommand()
        {
            var command = new Command("setup-update", "Actualiza la version de dtech")
            {
                new Option<string>(
                    new[] { "--version", "-v" },
                    getDefaultValue: () => defaultVersion,
                    description: "Indica la version a la cual actualizar por ejemplo 2.1.0")
            };

            command.Handler = CommandHandler.Create<string>(Handler);
            return command;
        }

        public static void Handler(string version)
        {
            try
            {
                if (HProcess.IsTheCorrectOS(OSPlatform.Linux, false)) UpdateToolLinux(version);
                if (HProcess.IsTheCorrectOS(OSPlatform.Windows, false)) UpdateToolWindows(version);
                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }


        #region SRP Methods
        public static void UpdateToolLinux(string version)
        {
            _ = version;
            HConsole.Error("Not supported yet!");
        }

        public static void UpdateToolWindows(string version)
        {
            HConsole.Title($"Updating tool");
            var title = "Write-Host \"Preparing to update...`n`n\";";
            var delay = "Start-Sleep -Milliseconds 1400;";
            var separator = "Write-Host \"`n`n\";";

            var nugetDir = Path.Combine(".", "core", "_Cli", "CliTool", "_nupkg");
            var v = version == defaultVersion ? "" : $" --version {version}";

            HProcess.RunProcessInNewWindow("powershell",
                title +
                delay +
                $"dotnet tool update -g --add-source {nugetDir} clitool{v};" +
                separator +
                "timeout /t -1", HDirectory.RootDirectory);
            HConsole.Warn("Process opened in other window.");
        }
        #endregion
    }
}
