﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._helpers
{
    public static class HDirectory // CrossPlatform herlpers
    {
        //Console.WriteLine(AppContext.BaseDirectory); // where the .exe file is
        //Console.WriteLine(Environment.CurrentDirectory); // where a command was run

        public static string CoreCodeFolder { get; } = ConfigurationManager.AppSettings[nameof(CoreCodeFolder)];
        public static string BaseContainerFolder { get; } = ConfigurationManager.AppSettings[nameof(BaseContainerFolder)];
        public static string CustomContainerFolder { get; } = ConfigurationManager.AppSettings[nameof(CustomContainerFolder)];

        public static string RootDirectory { get; set; } // path to rootDir
        public static bool IsRootDir { get; set; } // if command was run exactly in rootDir
        public static string SubPath { get; set; } // Path to (core,base,custom) when command was run inside any sub directory of that folders, es null si IsRoot es true
        public static SubPathKind? SubKind { get; set; }

        public static string SubPathFolderName { get; set; }

        public static void GetRootDirectory()
        {
            var current = Environment.CurrentDirectory;
            if (PathContainValidFolder(current))
            {
                while (current != null && !IsRootDirectory(current))
                    current = new DirectoryInfo(current).Parent?.FullName;
                RootDirectory = current; // null si no cumple, path si es un root 
            }
            else
            {
                RootDirectory = IsRootDirectory(current) ? current : null;
            }

            IsRootDir = RootDirectory == Environment.CurrentDirectory;
        }

        public static bool IsRunningInCorrectDirectory()
        {
            if (RootDirectory == null)
            {
                HConsole.Error("Wrong Directory! Please go to the folders where the system is located");
                return false;
            }
            return true;
        }

        public static void GetSubRootPath() => 
            SubPath = IsRootDir ? null : TrimPathToGetSubFolderPath(Environment.CurrentDirectory, 1);

        public static void SetPathKind()
        {
            if (IsRootDir)
            {
                SubKind = null;
            }
            else
            {
                var subPathName = new DirectoryInfo(SubPath).Name;
                if (subPathName == BaseContainerFolder || subPathName == CustomContainerFolder)
                    SubKind = SubPathKind.Container;
                else if (subPathName == CoreCodeFolder)
                    SubKind = SubPathKind.Core;
                else
                    SubKind = SubPathKind.Other;
            }
        }


        #region sub helpers
        public enum SubPathKind
        {
            Container, // command was run in a Container Folder, on SubPath or a SubPath's sub directory
            Core, // command was run in the Core Folder, on SubPath or a SubPath's sub directory
            Other 
        }

        private static string TrimPathToGetSubFolderPath(string path, int depthFromRoot)
        {
            var rootPathLength = SeparatePathToArray(RootDirectory).Length;
            return Path.Combine(SeparatePathToArray(path).Take(rootPathLength + depthFromRoot).ToArray());
        }

        private static string[] SeparatePathToArray(string path) // string to array, separated by hierarchy
        {
            var separatedFolders = path.Split(Path.DirectorySeparatorChar);
            var separatedFoldersAlt = path.Split(Path.AltDirectorySeparatorChar);
            if (separatedFolders.Length == 1 && separatedFoldersAlt.Length == 1)
                throw new InvalidOperationException("Incorrect path!");
            return separatedFolders.Length > 1 ? separatedFolders : separatedFoldersAlt;
        }

        private static bool PathContainValidFolder(string path) // path parents have at least 1 basic folder
        {
            foreach (var folderName in SeparatePathToArray(path))
                if (new[] { CoreCodeFolder, BaseContainerFolder, CustomContainerFolder }.Contains(folderName))
                    return true;
            return false;
        }

        private static bool IsRootDirectory(string path) // root is the folder which contains the basic folders (and .git repo)
        {
            var counter = 0;
            foreach (var folderFullPath in Directory.GetDirectories(path))
            {
                var folderName = new DirectoryInfo(folderFullPath).Name;
                counter += folderName == CoreCodeFolder ? 1 : 0;
                counter += folderName == BaseContainerFolder ? 1 : 0;
                counter += folderName == CustomContainerFolder ? 1 : 0;
            }
            return counter == 3;
        }
        #endregion

    }
}
