﻿using CliTool._classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CliTool._helpers.HDirectory;


namespace CliTool._helpers
{
    public static class HCore
    {
        public static Project DomainCoreProject { get; } = NewCoreProject("Domain", "domain.core");
        public static Project DomainGeneratorProject { get; } = NewCoreProject("Domain", "domain.generator");

        public static Project DomainProject { get; } = NewCoreProject("Domain", "domain");
        public static Project AppProject { get; } = NewCoreProject("Domain", "application"); 

        public static List<Project> BaseCoreProjects { get; } = new()
        {
            DomainCoreProject,
        };


        private static Project NewCoreProject(string group, string projectName)
        {
            var parentPath = Path.Combine(RootDirectory, CoreCodeFolder, group, projectName);
            var resProject = Project.New(projectName, parentPath);

            if (resProject.IsFailure)
                throw new InvalidOperationException(resProject.Error);
            if (!resProject.Value.Exist)
                throw new InvalidOperationException($"{projectName} no existe!");

            return resProject.Value;
        }
    }
}
