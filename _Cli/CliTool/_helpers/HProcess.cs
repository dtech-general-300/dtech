﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._helpers
{
    public static class HProcess
    {
        public static bool IsTheCorrectOS(OSPlatform os, bool printError = true)
        {
            var IsOS = RuntimeInformation.IsOSPlatform(os);
            if (!IsOS && printError)
                HConsole.Error("Sistema Operativo incorrecto!");
            return IsOS;
        }

        public static async Task RunProcess(string command, string args, string workingDirectory = null)
        {
            using var process = Init(command, args, workingDirectory);

            process.OutputDataReceived += (s, ea) => HConsole.Normal(ea.Data);
            process.ErrorDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                    HConsole.Error(ea.Data);
            };

            HConsole.Normal($"Run: {command} {args}");
            HConsole.Normal($"Working Directory: {workingDirectory ?? Environment.CurrentDirectory}");
            await Start(process);
        }

        public static async Task<List<string>> RunSilentProcess(string command, string args, string workingDirectory = null)
        {
            using var process = Init(command, args, workingDirectory);

            var res = new List<string>();
            var error = false;

            process.OutputDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                    res.Add(ea.Data);
            };

            process.ErrorDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                {
                    error = true;
                    HConsole.Error(ea.Data);
                }
            };

            await Start(process);

            if (error)
                throw new InvalidOperationException($"An Error Ocurred on: {workingDirectory} - Silent Process: {command} {args}");

            return res;
        }

        public static void RunProcessInNewWindow(string command, string args, string workingDirectory = null)
        {
            using var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = command,
                    Arguments = args,
                    UseShellExecute = true,
                    CreateNoWindow = false,
                    WindowStyle = ProcessWindowStyle.Normal,
                    WorkingDirectory = workingDirectory ?? Environment.CurrentDirectory
                }
            };

            HConsole.Normal($"Working Directory: {workingDirectory ?? Environment.CurrentDirectory}");
            process.Start();
        }


        #region sub helpers
        public static Process Init(string command, string args, string workingDirectory = null)
        {
            return new Process
            {
                EnableRaisingEvents = true,
                StartInfo = new ProcessStartInfo
                {
                    FileName = command,
                    Arguments = args,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WorkingDirectory = workingDirectory ?? Environment.CurrentDirectory
                    // if workingDir not supplied, use dir when this clitool was runned
                }
            };
        }

        public static async Task Start(Process process)
        {
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            await process.WaitForExitAsync();
        }
        #endregion
    }
}
