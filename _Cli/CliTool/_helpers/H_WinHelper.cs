﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._helpers
{
    public static class H_WinHelper // only for windows
    {
        #region Execution Policy
        public static async Task<string> GetExecutionPolicy(bool showLog = false)
        {
            using var process = HProcess.Init("powershell.exe", "Get-ExecutionPolicy -Scope LocalMachine");

            // the Output and Error delegates are called one time
            // with empty ea.Data when process finish its execution

            var result = new List<string>();
            process.OutputDataReceived += (s, ea) => result.Add(ea.Data);

            bool error = false;
            process.ErrorDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                {
                    error = true;
                    if (showLog)
                        HConsole.Error("ERR: " + ea.Data);
                }
            };

            await HProcess.Start(process);

            if (error || result.Count == 0)
                return "error";

            return result[0];
        }

        public static async Task SetExecutionPolicy(string policy, bool showLog = false)
        {
            using var process = HProcess.Init("powershell.exe", $"Set-ExecutionPolicy {policy} -Scope LocalMachine");

            process.OutputDataReceived += (s, ea) =>
            {
                if (showLog)
                    HConsole.Normal(ea.Data);
            };

            process.ErrorDataReceived += (s, ea) =>
            {
                if (showLog)
                    HConsole.Error("ERR: " + ea.Data);
            };

            await HProcess.Start(process);
        }
        #endregion

        //// Support for run powerShell script directly (not work well for all things)
        //public static async Task RunScript(string script) // powershell nuget
        //{
        //    using var ps = PowerShell.Create();
        //    ps.AddScript(script);
        //    var pipelineObjects = await ps.InvokeAsync().ConfigureAwait(false);

        //    // print the resulting pipeline objects to the console.
        //    foreach (var item in pipelineObjects)
        //        Console.WriteLine(item.BaseObject.ToString());
        //}

        public static async Task<string> GetProfileDir()
        {
            using var process = HProcess.Init("powershell.exe", "$PROFILE");

            var result = new List<string>();
            process.OutputDataReceived += (s, ea) => result.Add(ea.Data);

            bool error = false;
            process.ErrorDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                {
                    error = true;
                    HConsole.Error("ERR: " + ea.Data);
                }
            };

            await HProcess.Start(process);

            if (error || result.Count == 0)
                throw new InvalidOperationException(" Hubo un error al ejecutar el comando $PROFILE");

            return result[0];
        }

        public static bool IsAdministratorUser()
        {
            bool isAdmin = false;
            try
            {
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    WindowsIdentity user = WindowsIdentity.GetCurrent();
                    WindowsPrincipal principal = new(user);
                    isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
                }
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }


            if (isAdmin)
                HConsole.Good("Usuario es administrador");
            else
                HConsole.Error("Usuario no es administrador");

            return isAdmin;
        }
    }
}
