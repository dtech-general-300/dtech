﻿using CliTool._classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static CliTool._helpers.HDirectory;

namespace CliTool._helpers
{
    public static class HModule
    {
        public static Modulo Module { get; private set; }

        public static bool NewModulo(string moduleName, Container container)
        {
            var resModule = Modulo.New(moduleName, container.FolderPath);
            if (resModule.IsFailure)
            {
                HConsole.Error(resModule.Error);
                return false;
            }
            Module = resModule.Value;
            return true;
        }

        public static string CreateModuleDirectory(string moduleName)
        {
            HConsole.Title("Creating module directory");
            var moduleDir = Path.Combine(SubPath, moduleName);
            Directory.CreateDirectory(moduleDir); // create all non existing folders in a path
            HConsole.Normal("Done!");
            Console.WriteLine();
            return moduleDir;
        }

        #region paths
        public static string CorePath => Path.Combine(RootDirectory, CoreCodeFolder);

        public static string DomainPath => Path.Combine(CorePath, "Domain", "domain", "domain.csproj");
        public static string AppPath => Path.Combine(CorePath, "Application", "application", "application.csproj");

        public static string DomainGeneratorPath => Path.Combine(CorePath, "Domain", "domain.generator", "domain.generator.csproj");
        public static string DomainCorePath => Path.Combine(CorePath, "Domain", "domain.core", "domain.core.csproj");
        #endregion

        #region general
        

        public static bool VerifyConditionForUpdateAModule(string moduleName)
        {
            if (!ModuleExist(moduleName))
            {
                HConsole.Error($"Module doesn't exist!");
                return false;
            }
            //else if (!H_ContainerHelper.JsonExist())
            //{
            //    H_ColorHelper.Error($"Container config file doesn't exist!");
            //    return false;
            //}
            return true;
        }

        public static bool VerifyConditionForDeleteAModule(string moduleName)
        {
            if (!ModuleExist(moduleName))
            {
                HConsole.Error($"Module doesn't exist!");
                return false;
            }
            else if (!HContainer.SlnExist())
            {
                HConsole.Error($"Container solution file doesn't exist!");
                return false;
            }
            return true;
        }


        #endregion

        #region Zmodules
        public static string ToZFormat(string moduleName) =>
            moduleName.ToLower().First() == 'z' ? "z" + moduleName[1..] : "z" + moduleName;

        public static bool IsZmodule(string moduleName) =>
            moduleName.First() == 'z';
        #endregion


        #region Module SubProjects
        public static async Task<bool> IsProjectInSolution(string moduleName, string projectName, string workingDir)
        {
            var projectPath = Path.Combine(moduleName, projectName, $"{projectName}.csproj");

            var projectList = await HProcess.RunSilentProcess("dotnet", "sln list", workingDir);
            foreach (var project in projectList)
                if (project.Contains(projectPath))
                    return true;
            return false;
        }

        public static async Task AddProjectToSolution(string slnFolderPath, string projectPath, string workingDir) => 
            await HProcess.RunSilentProcess("dotnet", $"sln add -s {slnFolderPath} {projectPath}", workingDir);

        public static async Task RemoveProjectFromSolution(string projectPath, string workingDir) => 
            await HProcess.RunSilentProcess("dotnet", $"sln remove {projectPath}", workingDir);


        public static async Task<bool> IsProjectReferencedByOther(string principalProjectPath, string projectName)
        {
            var referencedProjectPath = Path.Combine(projectName, $"{projectName}.csproj");
            var projectList = await HProcess.RunSilentProcess("dotnet", $"list {principalProjectPath} reference");
            foreach (var project in projectList)
                if (project.Contains(referencedProjectPath))
                    return true;
            return false;
        }

        public static async Task AddRefenceToProject(string principalProjectPath, string projectPath) => 
            await HProcess.RunSilentProcess("dotnet", $"add {principalProjectPath} reference {projectPath}");

        public static async Task RemoveReferenceFromProject(string principalProjectPath, string projectPath) => 
            await HProcess.RunSilentProcess("dotnet", $"remove {principalProjectPath} reference {projectPath}");


        #region FinalMethods
        public static async Task AddProjectToCore(string moduleName, string projectName, string coreProjectPath)
        {
            var coreProjectName = Path.GetFileName(coreProjectPath);
            var projectPath = Path.Combine(SubPath, moduleName, projectName, $"{projectName}.csproj");

            if (!File.Exists(projectPath))
            {
                HConsole.Warn($"Este módulo no cotiene este project: {projectName}");
                return;
            }

            // solution
            if (await IsProjectInSolution(moduleName, projectName, CorePath))
                HConsole.Normal($"El project {projectName} ya está añadido a Core solución");
            else
            {
                var slnFolderPath = Path.Combine("_References", moduleName);
                await AddProjectToSolution(slnFolderPath, projectPath, CorePath);
                HConsole.Normal($"El project {projectName} ha sido añadido a Core Solution.");
            }


            // reference
            if (await IsProjectReferencedByOther(coreProjectPath, projectName))
                HConsole.Normal($"El project {projectName} ya está referenciado por Core Project {coreProjectName}");
            else
            {
                await AddRefenceToProject(coreProjectPath, projectPath);
                HConsole.Normal($"El project {projectName} ahora está referenciado por Core Project {coreProjectName}");
            }

        }

        public static async Task DeleteProjectFromCore(string moduleName, string projectName, string coreProjectPath)
        {
            var coreProjectName = Path.GetFileName(coreProjectPath);
            var projectPath = Path.Combine(SubPath, moduleName, projectName, $"{projectName}.csproj");

            if (!File.Exists(projectPath))
            {
                HConsole.Warn($"Este módulo no cotiene este project: {projectName}");
                return;
            }

            // reference
            if (await IsProjectReferencedByOther(coreProjectPath, projectName))
            {
                await RemoveReferenceFromProject(coreProjectPath, projectPath);
                HConsole.Normal($"El project {projectName} y su referencia han sido removidas de Core Project {coreProjectName}");
            }
            else
                HConsole.Normal($"El project {projectName} no estaba referenciado por Core Project {coreProjectName}");


            // solution
            if (await IsProjectInSolution(moduleName, projectName, CorePath))
            {
                await RemoveProjectFromSolution(projectPath, CorePath);
                HConsole.Normal($"El project {projectName} ha sido removido de Core Solution.");
            }
            else
                HConsole.Normal($"El project {projectName} no estaba añadido a Core Solution");


        }

        public static async Task DeleteProjectFromContainer(string moduleName, string projectName)
        {
            var projectPath = Path.Combine(SubPath, moduleName, projectName, $"{projectName}.csproj");

            if (!File.Exists(projectPath))
            {
                HConsole.Warn($"Este módulo no cotiene este project: {projectName}");
                return;
            }

            // solution
            if (await IsProjectInSolution(moduleName, projectName, SubPath))
            {
                await RemoveProjectFromSolution(projectPath, SubPath);
                HConsole.Normal($"El project {projectName} ha sido removido del Container Solution.");
            }
            else
                HConsole.Normal($"El project {projectName} no estaba añadido al Container Solution");


        }

        #endregion

        #endregion
    }
}
