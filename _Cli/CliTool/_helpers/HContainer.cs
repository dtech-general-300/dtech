﻿using CliTool._classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CliTool._helpers.HDirectory;

namespace CliTool._helpers
{
    public static class HContainer
    {
        public static Container Container { get; private set; }

        public static bool IsValidContainerSubFolder()
        {
            if (SubKind != SubPathKind.Container)
            {
                HConsole.Error($"Invalid Container SubDirectory! go to {BaseContainerFolder} or {CustomContainerFolder} folder");
                return false;
            }
            return true;
        }

        public static bool NewContainer()
        {
            var resContainer = Container.New(SubPathFolderName, RootDirectory);
            if (resContainer.IsFailure)
            {
                HConsole.Error(resContainer.Error);
                return false;
            }
            Container = resContainer.Value;
            return true;
        }

        public static string GetSlnName()
        {
            var slnPath = Directory.EnumerateFiles(SubPath, "*.sln").FirstOrDefault();
            if (slnPath == null)
                throw new InvalidOperationException("sln file doesn't exist! please create it!");
            return new DirectoryInfo(slnPath).Name;
        }

        public static bool SlnExist() => Directory.EnumerateFiles(SubPath, "*.sln").Any();

    }
}
