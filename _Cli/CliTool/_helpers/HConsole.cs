﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._helpers
{
    public static class HConsole // CrossPlatform herlpers
    {
        public static void Custom(string text, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public static void Title(string text) => Custom($">------ {text}", ConsoleColor.Yellow);

        public static void SubTitle(string text) => Custom($">{text}", ConsoleColor.White);
        
        public static void Normal(string text) => Custom(text, ConsoleColor.DarkGray);

        public static void Good(string text) => Custom(text, ConsoleColor.DarkGreen);

        public static void Error(string text) => Custom(text, ConsoleColor.DarkRed);
        
        public static void Warn(string text) => Custom(text, ConsoleColor.DarkCyan);

        public static void Exception(Exception e)
        {
            Custom($"\n>------ Error Happened!", ConsoleColor.Blue);
            Custom($"\nMessage:{e.Message}\n", ConsoleColor.Gray);
            Custom("StackTrace:", ConsoleColor.Red);
            Custom(e.StackTrace, ConsoleColor.Red);
            Custom($"\n\nMessage:{e.Message}\n\n", ConsoleColor.Gray);
        }
    }
}
