﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._helpers
{
    public static class HTemplates // CrossPlatform herlpers
    {
        public static string DomainTemplateName { get; } = ConfigurationManager.AppSettings["DomainTemplateName"];
        public static string AppTemplateName { get; } = ConfigurationManager.AppSettings["AppTemplateName"];
        
        public static string ZDomainTemplateName { get; } = ConfigurationManager.AppSettings["ZDomainTemplateName"];
        public static string ZAppTemplateName { get; } = ConfigurationManager.AppSettings["ZAppTemplateName"];

        public static string DomainPrefix { get; } = ConfigurationManager.AppSettings["DomainPrefix"];
        public static string AppPrefix { get; } = ConfigurationManager.AppSettings["AppPrefix"];

        public static string GetDomainProjectName(string moduleName) => $"{DomainPrefix}{moduleName}";
        public static string GetAppProjectName(string moduleName) => $"{AppPrefix}{moduleName}";

        public static async Task<List<string>> InstallTemplates(string path, bool showLog = true)
        {
            using var process = HProcess.Init("dotnet", $"new -i {path}");

            // the Output and Error delegates are called one time
            // with empty ea.Data when process finish its execution

            var result = new List<string>();
            process.OutputDataReceived += (s, ea) => result.Add(ea.Data);

            bool error = false;
            process.ErrorDataReceived += (s, ea) =>
            {
                if (!string.IsNullOrEmpty(ea.Data))
                {
                    error = true;
                    if (showLog)
                        HConsole.Error("ERR: " + ea.Data);
                }
            };

            await HProcess.Start(process);

            if (error)
                throw new InvalidOperationException("Errores durante la instalación de custom templates");

            HConsole.Normal("Done!");
            Console.WriteLine();
            return result;
        }

        public static async Task CreateProjectFromTemplate(
            string templateName, string projectName, string workingDir, string args = null)
        {
            args = args == null ? "" : $" {args}"; // prefix space
            HConsole.Title($"Creating project with the template: {templateName}");
            await HProcess.RunProcess("dotnet", $"new {templateName} -n {projectName}{args}", workingDir);
        }

    }
}
