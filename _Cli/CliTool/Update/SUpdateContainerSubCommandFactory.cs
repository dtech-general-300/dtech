﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Update
{
    public class SUpdateContainerSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("container",
                "Actualiza-Instala-Desinstala todos los módulos de un solo container, con sus dependencias. " +
                "Lo hace basado en su archivos de configuracion .json")
            {
                new Argument<string>(
                    name: "name",
                    description: "El nombre utilizado para el contenedor de modulos y su configuracion")
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            command.AddAlias("c");
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {

                await Task.Delay(100);

                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }
    }
}
