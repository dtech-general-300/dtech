﻿using CliTool._classes._factories;
using CliTool._helpers;
using System.CommandLine;

namespace CliTool.Update
{
    public class UpdateCommandFactory : CommandFactory
    {
        public override Command CreateCommand()
        {
            var command = new Command("update", 
                "Actualiza-Instala-Desinstala módulos y containers, " +
                $"estas acciones son realizadas en el core del sistema {HDirectory.CoreCodeFolder}")
            {
                //new SUpdateAllSubCommandFactory().GetSubCommand(),
                //new SUpdateContainerSubCommandFactory().GetSubCommand(),
                new SUpdateModuleSubCommandFactory().GetSubCommand(),
            };
            command.AddAlias("u");
            return command;
        }
    }
}
