﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Update
{
    public class SUpdateAllSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("all",
                "Actualiza-Instala-Desinstala todos los módulo y sus dependecias, " +
                "de todos los containers " +
                "basado en sus archivos de configuracion .json..............")
            {
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {

                var profileDir = await H_WinHelper.GetProfileDir();
                Console.WriteLine("--------------");
                Console.WriteLine(profileDir);
                Console.WriteLine("--------------");

                //await Task.Delay(100);

                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }
    }
}
