﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;

namespace CliTool.Update
{
    public class SUpdateModuleSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("module",
                "Actualiza el core con los proyectos de un solo módulo")
            {
                new Argument<string>(
                    name: "name",
                    description: "nombre del módulo a actualizar "),
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            command.AddAlias("m");
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {
                if (!HContainer.IsValidContainerSubFolder()) return;
                //if (!H_ModuleHelper.IsValidModuleName(name)) return;
                if (!HModule.VerifyConditionForUpdateAModule(name)) return;

                HConsole.Title("Add projects to core");
                await AddDomain(name);
                await AddApplication(name);

                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }


        #region SRP Methods
        public static async Task AddDomain(string moduleName)
        {
            HConsole.SubTitle("Domain Project");
            var projectName = HTemplates.GetDomainProjectName(moduleName);
            await HModule.AddProjectToCore(moduleName, projectName, HModule.DomainPath);
        }

        public static async Task AddApplication(string moduleName)
        {
            HConsole.SubTitle("Application Project");
            var projectName = HTemplates.GetAppProjectName(moduleName);
            await HModule.AddProjectToCore(moduleName, projectName, HModule.AppPath);
        }
        #endregion
    }
}
