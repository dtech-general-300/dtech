﻿using CliTool._classes;
using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Generate
{
    public class NewModuleSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("module", "genera un módulo estándar tipo admin")
            {
                new Argument<string>(
                    name: "name",
                    description: "nombre del módulo"),
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            command.AddAlias("m");
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {
                if (!HModule.NewModulo(name, HContainer.Container)) return;                

                if (!HContainer.Container.Solution.Exist)
                {
                    HConsole.Error($"Containter solution doesn't exist!");
                    return;
                }

                if (HModule.Module.Exist)
                {
                    HConsole.Error($"Module Directory Already exists!");
                    return;
                }

                await HModule.Module.CreateStandardModule(HContainer.Container.Solution);
                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }

    }
}
