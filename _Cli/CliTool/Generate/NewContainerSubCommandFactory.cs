﻿using CliTool._classes;
using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CliTool.Generate
{
    public class NewContainerSubCommandFactory : ContainerSubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("container", "genera un contenedor que es una solucion de dotnet que contiene modulos y configuración")
            {
            };
            command.Handler = CommandHandler.Create(Handler);
            command.AddAlias("c");
            return command;
        }

        public static async Task Handler()
        {
            try
            {
                await HContainer.Container.CreateConfig();
                await HContainer.Container.CreateSolution();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }

    }
}
