﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Generate
{
    public class NewModuleZSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("zmodule", "genera un módulo estándar tipo zero")
            {
                new Argument<string>(
                    name: "name",
                    description: "nombre del módulo zero"),
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            command.AddAlias("mz");
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {
                if (!HContainer.IsValidContainerSubFolder()) return;
                if (!HModule.VerifyConditionsForANewModule(name)) return;
                //if (!H_ModuleHelper.IsValidModuleName(name)) return;

                await CreateModuleZ(name);

                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }

        #region SRP Methods
        public static async Task CreateModuleZ(string moduleName)
        {
            moduleName = HModule.ToZFormat(moduleName);
            var moduleDir = HModule.CreateModuleDirectory(moduleName);

            await CreateZDomainProject(moduleName, moduleDir);
            await CreateZAppProject(moduleName, moduleDir);

            var slnName = HContainer.GetSlnName();
            HConsole.Title($"Add modules to: {slnName}");
            await HModule.IncludeModuleProjectsInSolution(moduleDir, slnName);
        }

        private static async Task CreateZAppProject(string moduleName, string moduleDir) =>
            await HTemplates.CreateProjectFromTemplate(
                HTemplates.ZAppTemplateName,
                HTemplates.GetAppProjectName(moduleName),
                moduleDir);

        private static async Task CreateZDomainProject(string moduleName, string moduleDir)
        {
            var projectName = HTemplates.GetDomainProjectName(moduleName);
            var projectPath = Path.Combine(moduleDir, projectName);

            var relativeDomainCorePath = Path.GetRelativePath(projectPath, HModule.DomainCorePath);
            var relativeDomainGeneratorPath = Path.GetRelativePath(projectPath, HModule.DomainGeneratorPath);

            await HTemplates.CreateProjectFromTemplate(
                HTemplates.ZDomainTemplateName,
                projectName,
                moduleDir,
                $"-D {relativeDomainCorePath} -Do {relativeDomainGeneratorPath}");
        }
        #endregion

    }
}
