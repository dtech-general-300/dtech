﻿using CliTool._classes._factories;
using System.CommandLine;

namespace CliTool.Generate
{
    public class GenerateCommandFactory : CommandFactory
    {
        public override Command CreateCommand()
        {
            var command = new Command("generate", "Genera varios tipos de archivos en base a parametros ingresados")
            {
                new NewContainerSubCommandFactory().GetSubCommand(),
                new NewModuleSubCommandFactory().GetSubCommand(),
                new NewModuleZSubCommandFactory().GetSubCommand(),
            };
            command.AddAlias("g");
            return command;
        }
    }
}
