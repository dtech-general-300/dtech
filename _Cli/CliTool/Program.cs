﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.CommandLine;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CliTool
{
    class Program
    {
        static async Task Main(string[] args)
        {
            #region Initialization and Validations
            try
            {
                HDirectory.GetRootDirectory();
                if (!HDirectory.IsRunningInCorrectDirectory()) return;
                HDirectory.GetSubRootPath();
                HDirectory.SetPathKind();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
            #endregion

            #region Application code
            var commands = Assembly.GetExecutingAssembly().GetTypes()
                .Where(t => t.BaseType == typeof(CommandFactory))
                .Select(t => ((CommandFactory)Activator.CreateInstance(t)).GetCommand());

            var root = new RootCommand("ABM Sistemas");

            foreach (var command in commands)
                root.AddCommand(command);

            await root.InvokeAsync(args);
            #endregion
        }
    }
}
