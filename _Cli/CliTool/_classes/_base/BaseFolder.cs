﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CliTool._classes._base
{
    public abstract class BaseFolder
    {
        public string FolderName { get; protected set; }
        public string FolderPath => Path.Combine(ParentPath, FolderName);
        
        public string ParentPath { get; protected set; }
        public string ParentFolderName => new DirectoryInfo(ParentPath).Name;

        public bool Exist => Directory.Exists(FolderPath);

        protected BaseFolder(string folderName, string parentPath)
        {
            FolderName = folderName;
            ParentPath = parentPath;
        }

        public static Result BasicNameAndParentPathValidation(string folderName, string parentPath)
        {
            if ((new Regex("[^a-zA-Z0-9._-]")).IsMatch(folderName))
                return Result.Fail<Container>("Una carpeta no puede ser nombrada con Caracteres especiales.");

            if (!Directory.Exists(parentPath))
                return Result.Fail<Container>("El directorio padre de la carpeta no existe.");

            return Result.Ok();
        }

        #region Methods

        public string RelativeFolderPath(string relativeTo) =>
            Path.GetRelativePath(relativeTo, FolderPath);

        #endregion
    }
}
