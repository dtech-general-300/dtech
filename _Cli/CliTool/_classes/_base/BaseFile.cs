﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CliTool._classes._base
{
    public abstract class BaseFile
    {
        public string FileName { get; protected set; }
        public string FileExtension { get; protected set; }
        public string FileNameAndExt => FileName + FileExtension;
        public string FilePath => Path.Combine(ParentPath, FileNameAndExt);

        public string ParentPath { get; protected set; }
        public string ParentFolderName => new DirectoryInfo(ParentPath).Name;

        public bool Exist => File.Exists(FilePath);

        protected BaseFile(string fileName, string ext, string parentPath)
        {
            FileName = fileName;
            ParentPath = parentPath;
            FileExtension = ext;
        }

        public static Result BasicNameAndParentPathValidation(string fileName, string parentPath)
        {
            if ((new Regex("[^a-zA-Z0-9._-]")).IsMatch(fileName))
                return Result.Fail("Un archivo no puede tener Caracteres especiales.");

            if (!Directory.Exists(parentPath))
                return Result.Fail("El directorio padre del archivo no existe.");

            return Result.Ok();
        }

        #region Methods
        
        public string Relative(string relativeTo) =>
            Path.GetRelativePath(relativeTo, FilePath);

        #endregion
    }
}
