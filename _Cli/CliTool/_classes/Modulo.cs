﻿using CliTool._classes._base;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CliTool._classes
{
    public sealed class Modulo : BaseFolder
    {
        public Container Container { get; private set; }

        private List<Project> _existing_projects = new();
        public IReadOnlyCollection<Project> Projects => _existing_projects;


        #region Factory
        private Modulo(string name, string parentPath)
            : base(name, parentPath) { }

        public static Result<Modulo> New(string moduleName, string parentPath)
        {
            var result = Result.Combine(
                BasicNameAndParentPathValidation(moduleName, parentPath),
                IsValidModuleName(moduleName));
            if (result.IsFailure)
                return Result.Fail<Modulo>(result.Error);

            var module = new Modulo(moduleName, parentPath);
            module.DetectProjects();

            return Result.Ok(module);
        }

        public static Result IsValidModuleName(string moduleName)
        {
            if (moduleName[0] == '.')
                Result.Fail("Los modulos no son nombrados con '.' punto al inicio!");
            return Result.Ok();
        }

        private void DetectProjects()
        {
            _existing_projects = new();

            if (!Exist)
                return;

            foreach (var projectFolderPath in Directory.GetDirectories(FolderPath))
            {
                var projectName = new DirectoryInfo(projectFolderPath).Name;
                var result = Project.New(projectName, projectFolderPath);
                if (result.IsSuccess && result.Value.Exist)
                    _existing_projects.Add(result.Value);
            }
        }
        #endregion


        #region Create Standard
        public async Task CreateStandardModule(Solution solution)
        {
            HConsole.Title("Creating module directory");
            Directory.CreateDirectory(FolderPath); // create all non existing folders in a path
            HConsole.Normal("Done!");
            Console.WriteLine();

            await CreateStandardProjects();

            HConsole.Title($"Add modules to: {solution.FileName}");
            await AddProjectsToSolution(solution);
        }

        private async Task CreateStandardProjects()
        {
            // domain
            var domainProjectName = HTemplates.GetDomainProjectName(FolderName);
            var domainProjectPath = Path.Combine(FolderPath, domainProjectName);
            var resModuleDomain = Project.New(domainProjectName, domainProjectPath);
            if (resModuleDomain.IsFailure)
            {
                HConsole.Error(resModuleDomain.Error);
                return;
            }

            await HTemplates.CreateProjectFromTemplate(
                        HTemplates.DomainTemplateName,
                        resModuleDomain.Value.FileName,
                        FolderPath,
                        $"-D {HCore.DomainCoreProject.Relative(domainProjectPath)} " +
                        $"-Do {HCore.DomainGeneratorProject.Relative(domainProjectPath)}");

            
            // aplication
            var appProjectName = HTemplates.GetDomainProjectName(FolderName);
            var appProjectPath = Path.Combine(FolderPath, appProjectName);
            var resModuleApp = Project.New(appProjectName, appProjectPath);
            if (resModuleApp.IsFailure)
            {
                HConsole.Error(resModuleApp.Error);
                return;
            }
            await HTemplates.CreateProjectFromTemplate(
                        HTemplates.AppTemplateName,
                        resModuleApp.Value.FileName,
                        FolderPath);
        }


        #endregion

        #region Projects
        public async Task<bool> AddProjectsToSolution(Solution solution)
        {
            HConsole.SubTitle($"Add: {FolderName}");
            if (_existing_projects.Count == 0)
            {
                HConsole.Warn($"{FolderName}: There are no projects to add, inside module folder!");
                Console.WriteLine();
                return false;
            }

            var args = $"sln {solution.FileNameAndExt} add -s {FolderName}";
            foreach (var project in _existing_projects)
                args += " " + Path.Combine(".", FolderName, project.ParentFolderName, project.FileNameAndExt);

            await HProcess.RunProcess("dotnet", args, ParentPath);
            return true;
        }
        #endregion

    }
}

