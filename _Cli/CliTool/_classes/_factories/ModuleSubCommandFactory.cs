﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._classes._factories
{
    public abstract class ModuleSubCommandFactory
    {
        public abstract Command CreateSubCommand();

        public Command GetSubCommand()
        {
            if (!HContainer.IsValidContainerSubFolder()) return new Command("m");
            if (!HContainer.NewContainer()) return new Command("m");

            var command = CreateSubCommand();
            return command;
        }
    }
}
