﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._classes._factories
{
    public abstract class SubCommandFactory // FACTORY METHOD PATTERN
    {
        public abstract Command CreateSubCommand();

        public Command GetSubCommand()
        {
            var command = CreateSubCommand();
            return command;
        }
    }
}
