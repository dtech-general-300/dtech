﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._classes._factories
{
    public abstract class CommandFactory // FACTORY METHOD PATTERN
    {
        public abstract Command CreateCommand();

        public Command GetCommand()
        {
            var command = CreateCommand();
            return command;
        }
    }
}
