﻿using CliTool._classes._base;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CliTool._classes
{
    public class Container : BaseFolder
    {
        public Solution Solution { get; private set; }
        public Settings Settings { get; private set; }

        private List<Modulo> _existing_modules = new();
        public IReadOnlyCollection<Modulo> Modules => _existing_modules;


        #region Factory
        private Container(string name, string parentPath)
            : base(name, parentPath) { }

        public static Result<Container> New(string name, string parentPath)
        {
            var result = BasicNameAndParentPathValidation(name, parentPath);
            if (result.IsFailure)
                return Result.Fail<Container>(result.Error);

            var container = new Container(name, parentPath);
            container.DetectModules();

            var resFiles = container.InstantiateMainFiles();
            if(resFiles.IsFailure)
                return Result.Fail<Container>(resFiles.Error);

            return Result.Ok(container);
        }

        private void DetectModules()
        {
            _existing_modules = new();

            if (!Exist)
                return;

            foreach (var moduleFolderPath in Directory.GetDirectories(FolderPath))
            {
                var moduleName = new DirectoryInfo(moduleFolderPath).Name;
                var result = Modulo.New(moduleName, FolderPath);
                if (result.IsSuccess)
                    _existing_modules.Add(result.Value);
            }
        }

        private Result InstantiateMainFiles()
        {
            var resSol = Solution.New(FolderName, FolderPath);
            if (resSol.IsFailure)
                return Result.Fail(resSol.Error);

            var resSett = Settings.New($"appsettings.{FolderName}", FolderPath);
            if (resSett.IsFailure)
                return Result.Fail(resSett.Error);

            Solution = resSol.Value;
            Settings = resSett.Value;
            return Result.Ok();
        }
        #endregion


        #region Creation
        public async Task CreateConfig()
        {
            HConsole.Title("Creating config file");
            if (Settings.Exist)
                HConsole.Error($"Config file Already exists!");
            else
            {
                await Settings.CreateSettingsFile(new SettingsModel());
                HConsole.Normal("Config created!");
            }
            Console.WriteLine();
        }

        public async Task CreateSolution()
        {
            HConsole.Title("Creating solution");
            if (Solution.Exist)
                HConsole.Error($"Solution Already exists!");
            else
            {
                await Solution.CreateSolutionFile();

                await AddExistingModulesToSolution();
                await AddCoreProjectsToSolution();
            }
            Console.WriteLine();

        }

        private async Task AddExistingModulesToSolution()
        {
            HConsole.Title($"Add modules to: {Solution.FileName}");
            
            var moduleCounter = 0;
            foreach (var module in _existing_modules)
                if(await module.AddProjectsToSolution(Solution))
                    moduleCounter++;

            if (moduleCounter == 0)
                HConsole.Warn("No se ha añadido ningún módulo.");

            Console.WriteLine();
        }

        private async Task AddCoreProjectsToSolution()
        {
            HConsole.Title($"Add core projects to: {Solution.FileName}");

            if (HCore.BaseCoreProjects.Count == 0)
            {
                HConsole.Error($"There are no core projects to add to solution!");
                Console.WriteLine();
                return;
            }

            var args = $"sln {Solution.FileNameAndExt} add -s _core";
            foreach (var project in HCore.BaseCoreProjects)
                args += " " + project.Relative(FolderPath);

            await HProcess.RunProcess("dotnet", args, FolderPath);
        }
        #endregion


        #region Modules
        public bool HasModule(string moduleName)
        {
            return _existing_modules.Select(m => m.FolderName).Contains(moduleName);
        }
        #endregion
    }
}
