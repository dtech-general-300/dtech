﻿using CliTool._classes._base;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._classes
{
    public class Project : BaseFile
    {
        #region Factory
        private Project(string projectName, string parentPath)
            : base(projectName, ".csproj", parentPath) { }

        public static Result<Project> New(string projectName, string parentPath)
        {
            var result = BasicNameAndParentPathValidation(projectName, parentPath);
            if (result.IsFailure)
                return Result.Fail<Project>(result.Error);

            var project = new Project(projectName, parentPath);

            return Result.Ok(project);
        }
        #endregion
    }
}
