﻿using CliTool._classes._base;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CliTool._classes
{
    public class Settings : BaseFile
    {
        #region Factory
        private Settings(string settingsName, string parentPath)
            : base(settingsName, ".json", parentPath) { }

        public static Result<Settings> New(string settingsName, string parentPath)
        {
            var result = BasicNameAndParentPathValidation(settingsName, parentPath);
            if (result.IsFailure)
                return Result.Fail<Settings>(result.Error);

            var solution = new Settings(settingsName, parentPath);

            return Result.Ok(solution);
        }
        #endregion

        public async Task CreateSettingsFile<T>(T model)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            var json = JsonSerializer.Serialize(model, options);
            await File.WriteAllTextAsync(FilePath, json);
        }

    }
}
