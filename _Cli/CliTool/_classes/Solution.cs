﻿using CliTool._classes._base;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool._classes
{
    public class Solution : BaseFile
    {
        #region Factory
        private Solution(string solutionName, string parentPath)
            : base(solutionName, ".sln", parentPath) { }

        public static Result<Solution> New(string solutionName, string parentPath)
        {
            var result = BasicNameAndParentPathValidation(solutionName, parentPath);
            if (result.IsFailure)
                return Result.Fail<Solution>(result.Error);

            var solution = new Solution(solutionName, parentPath);

            return Result.Ok(solution);
        }
        #endregion

        public async Task CreateSolutionFile()
        {
            await HProcess.RunProcess("dotnet", $"new sln -n {FileName}", ParentPath);
        }
    }
}
