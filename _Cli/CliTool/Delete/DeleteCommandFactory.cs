﻿using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Update
{
    public class DeleteCommandFactory : CommandFactory
    {
        public override Command CreateCommand()
        {
            var command = new Command("delete", 
                "Desinstala módulos y containers")
            {
                //new SUpdateAllSubCommandFactory().GetSubCommand(),
                //new SUpdateContainerSubCommandFactory().GetSubCommand(),
                new SDeleteModuleSubCommandFactory().GetSubCommand(),
            };
            command.AddAlias("d");
            return command;
        }
    }
}
