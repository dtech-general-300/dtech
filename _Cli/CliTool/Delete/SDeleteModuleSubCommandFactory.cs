﻿using CliTool._classes._factories;
using CliTool._helpers;
using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CliTool.Update
{
    public class SDeleteModuleSubCommandFactory : SubCommandFactory
    {
        public override Command CreateSubCommand()
        {
            var command = new Command("module",
                "Elimina los archivos del módulo, deshace todas las referencias del mismo")
            {
                new Argument<string>(
                    name: "name",
                    description: "nombre del módulo a eliminar"),
            };
            command.Handler = CommandHandler.Create<string>(Handler);
            command.AddAlias("m");
            return command;
        }

        public static async Task Handler(string name)
        {
            try
            {
                if (!HContainer.IsValidContainerSubFolder()) return;
                //if (!H_ModuleHelper.IsValidModuleName(name)) return;
                if (!HModule.VerifyConditionForDeleteAModule(name)) return;

                HConsole.Title("Eliminar del core");
                await DeleteDomainFromCore(name);
                await DeleteApplicationFromCore(name);

                HConsole.Title("Eliminar del container");
                await DeleteDomainFromContainer(name);
                await DeleteApplicationFromContainer(name);

                HConsole.Title("Eliminar archivos");
                DeleteModuleFromDirectory(name);

                Console.WriteLine();
            }
            catch (Exception e)
            {
                HConsole.Exception(e);
            }
        }


        #region SRP Methods
        public static async Task DeleteDomainFromCore(string moduleName)
        {
            HConsole.SubTitle("Domain Project");
            var projectName = HTemplates.GetDomainProjectName(moduleName);
            await HModule.DeleteProjectFromCore(moduleName, projectName, HModule.DomainPath);
            Console.WriteLine();
        }

        public static async Task DeleteApplicationFromCore(string moduleName)
        {
            HConsole.SubTitle("Application Project");
            var projectName = HTemplates.GetAppProjectName(moduleName);
            await HModule.DeleteProjectFromCore(moduleName, projectName, HModule.AppPath);
            Console.WriteLine();
        }



        public static async Task DeleteDomainFromContainer(string moduleName)
        {
            HConsole.SubTitle("Domain Project");
            var projectName = HTemplates.GetDomainProjectName(moduleName);
            await HModule.DeleteProjectFromContainer(moduleName, projectName);
            Console.WriteLine();
        }

        public static async Task DeleteApplicationFromContainer(string moduleName)
        {
            HConsole.SubTitle("Application Project");
            var projectName = HTemplates.GetAppProjectName(moduleName);
            await HModule.DeleteProjectFromContainer(moduleName, projectName);
            Console.WriteLine();
        }



        public static void DeleteModuleFromDirectory(string moduleName)
        {
            var modulePath = new DirectoryInfo(Path.Combine(HDirectory.SubPath, moduleName));

            if (modulePath.Exists)
            {
                modulePath.Delete(true);
                HConsole.Normal($"Directorios y archivos de {moduleName} eliminados completamente.");
            }
            Console.WriteLine();
        }

        #endregion
    }
}
