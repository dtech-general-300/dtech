﻿using FluentAssertions;
using domain.generator.C_Source;
using domain.generator.Helpers;
using domain.generator.tests.TClasses;
using Xunit;

namespace domain.generator.tests
{
    // each Fact create a new instance of this class
    public class C1_Usings_Should
    {
        // if you change this prop, may affect several tests
        private Usings TestUsings { get; set; } = new();

        [Fact]
        public void BeCreatedWith_Empty_InsertedHashet()
        {
            TestUsings.Inserted.Should().BeEmpty();
        }

        [Fact]
        public void BeCreatedWith_Empty_StringBuilderHeader()
        {
            TestUsings.Header.ToString().Should().BeEmpty();
        }

        [Fact]
        public void Return_EmptyHeaderString_With_EmptyInserted()
        {
            TestUsings.HeaderString.Should().BeEmpty();
        }

        #region AddUsing
        [Fact]
        public void AddUsing_If_ItWasNot_InsertedYet()
        {
            TestUsings.AddUsing("System");

            TestUsings.Inserted.Should().HaveCount(1);
        }

        [Fact]
        public void NotAddUsing_If_ItWasAlready_Inserted()
        {
            TestUsings.AddUsing("Xunit");
            TestUsings.AddUsing("Xunit");

            TestUsings.Inserted.Should().HaveCount(1);
        }

        [Fact]
        public void Have_Correct_CSharpFormat_For_Usings()
        {
            TestUsings.AddUsing("System.Text");

            TestUsings.HeaderString.Should().Contain("using System.Text;");
        }
        #endregion

    }
}