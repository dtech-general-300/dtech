﻿using FluentAssertions;
using domain.generator.Helpers;
using domain.generator.tests.TClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace domain.generator.tests
{
    public class A2_IsDddNamespace_Should
    {
        [Theory]
        [BadLengthNamespaces]
        public void ReturnFalse_If_NamespaceHasNot_TheMinimunLength(string nameSpace)
        {
            var result = Folders.IsDddNamespace(nameSpace, out _, out _);

            result.Should().BeFalse();
        }

        [Theory]
        [ValidDddNamespaces]
        public void ReturnTrue_If_NamespaceHas_TheMinimunLength(string nameSpace)
        {
            var result = Folders.IsDddNamespace(nameSpace, out _, out _);

            result.Should().BeTrue();
        }

        [Theory]
        [ValidDddNamespaces]
        public void ReturnTheModuleName_AtIndex_1(string nameSpace)
        {
            var ModuleName = nameSpace.Split('.')[1];

            _ = Folders.IsDddNamespace(nameSpace, out var moduleName, out _);

            moduleName.Should().Be(ModuleName);
        }

        [Theory]
        [ValidDddNamespaces]
        public void ReturnTheSubFolderName_AtIndex_2(string nameSpace)
        {
            var SubFolderName = nameSpace.Split('.')[2];

            _ = Folders.IsDddNamespace(nameSpace, out _, out var subFolderName);

            subFolderName.Should().Be(SubFolderName);
        }

        [Theory]
        [ValidDddNamespaces]
        public void Return_A_DDDSubFolderName_For_ValidNamespaces(string nameSpace)
        {
            _ = Folders.IsDddNamespace(nameSpace, out _, out var subFolderName);

            Data.Map.Keys.Should().Contain(subFolderName);
        }


    }
}
