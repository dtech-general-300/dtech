﻿using FluentAssertions;
using domain.generator.Helpers;
using domain.generator.tests.TClasses;
using System.Text;
using Xunit;

namespace domain.generator.tests
{
    public class C5_CreateNamespace_Should
    {
        private const string content = "// nothing";
        private const string moduleName = "MyModule";

        [Fact]
        public void Return_A_Namespace_With_NoIndent()
        {
            var result = Code.CreateNamespace(moduleName, content);

            result.Should().StartWith("namespace ");
        }

        [Fact]
        public void Return_The_Correct_GeneratorNamespaceFormat()
        {
            var sb = new StringBuilder();
            sb.AppendLine($" {Data.GeneratorFiles_NamespaceFormat}{moduleName}");

            var result = Code.CreateNamespace(moduleName, content);

            result.Should().Contain(sb.ToString());
        }

    }
}
