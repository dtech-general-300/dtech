﻿using FluentAssertions;
using domain.generator.B_Info;
using domain.generator.Helpers;
using domain.generator.tests.TClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace domain.generator.tests
{
    public class C2_ToStringMethod_Should
    {
        private const string moduleName = "TestModule";
        public static BTypeInfo FirstPropTypeOfFirstClass => DddPropsDataAttribute.types.First();
        public static BTypeInfo AnyNotDddPropType => NotDddPropsDataAttribute.types.First();
        private Usings TestUsings { get; set; } = new();

        [Theory]
        [BuiltInPropsData]
        public void Return_A_BuiltInTypeString_For_BuiltInTypes(BTypeInfo type)
        {
            var builtInType = Code.ToString(type, moduleName, null);

            Data.builtInTypes.Should().Contain(builtInType);
        }

        #region DddProps
        [Theory]
        [DddPropsData]
        public void Return_A_CustomDtoFormatString_For_DDDProperties(BTypeInfo type, string expectedResult)
        {
            var DtoType = Code.ToString(type, moduleName, null);

            DtoType.Should().Be(expectedResult);
        }

        [Fact]
        public void NotAddUsing_For_DDDProperties_OfTheSameModule()
        {
            Code.ToString(FirstPropTypeOfFirstClass, moduleName, TestUsings);

            TestUsings.Inserted.Should().HaveCount(0);
        }

        [Fact]
        public void AddUsing_For_DDDProperties_OfDifferentModule()
        {
            Code.ToString(FirstPropTypeOfFirstClass, "OtherModule", TestUsings);

            TestUsings.Inserted.Should().HaveCount(1);
        }

        [Fact]
        public void AddUsing_OfSpecificFormat_For_DDDProperties_OfDifferentModule()
        {
            Code.ToString(FirstPropTypeOfFirstClass, "OtherModule", TestUsings);

            TestUsings.HeaderString.Should().Contain(Data.GeneratorFiles_NamespaceFormat);
        }
        #endregion


        #region NotDddProps
        [Fact]
        public void AddUsing_For_NotDDDProperties()
        {
            Code.ToString(AnyNotDddPropType, null, TestUsings);

            TestUsings.Inserted.Should().HaveCount(1);
        }

        [Theory]
        [NotDddPropsData]
        public void AddUsing_With_TypeNamespace_For_NotDDD_And_NotBuiltIn_Props(BTypeInfo type, string expectedResult)
        {
            var usings = new Usings();

            Code.ToString(type, null, usings);

            usings.HeaderString.Should().Contain(expectedResult);
        }

        [Fact]
        public void Change_IReadOnlyCollectionType_To_List_For_NotDDDProperties()
        {
            var readonlyType = NotDddPropsDataAttribute.types[2]; // IReadOnlyCollection

            var result = Code.ToString(readonlyType, null, TestUsings);

            result.Should().Be("List<string>");
        }
        #endregion

        #region Generics
        [Theory]
        [GenericPropsData]
        public void Return_A_String_With_ExactNumberOf_LessMoreThan_CharSymbols_For_Generics(BTypeInfo type)
        {
            var usings = new Usings();
            static int Levels(BTypeInfo iType)
            {
                var i = iType.IsGeneric ? 1 : 0;
                foreach (var arg in iType.GenericArguments)
                    i += Levels(arg);
                return i;
            }

            var result = Code.ToString(type, null, usings);
            var levels = Levels(type);

            var nTimes = Exactly.Times(levels);
            result.Should().Contain("<", nTimes).And.Contain(">", nTimes);
        }

        [Theory]
        [GenericPropsData]
        public void Return_A_String_With_ExactNumberOf_Comma_CharSymbols_For_Generics(BTypeInfo type)
        {
            var usings = new Usings();
            static int NumberOfCommas(BTypeInfo iType)
            {
                var count = iType.GenericArguments.Count;
                count = count == 0 ? 0 : count - 1;
                foreach (var arg in iType.GenericArguments)
                    count += NumberOfCommas(arg);
                return count;
            }

            var result = Code.ToString(type, null, usings);
            var commas = NumberOfCommas(type);

            result.Should().Contain(",", Exactly.Times(commas));
        }

        #endregion
    }
}
