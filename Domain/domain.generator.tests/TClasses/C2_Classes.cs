﻿using Project.TestModule.Aggregates.AggregateName1;
using Project.TestModule.Aggregates.AggregateName2;
using Project.TestModule.ValueObjects;
using contoso.generics;
using contoso.irrealnamespace;
using System;
using System.Collections.Generic;


// DDD PROPERTIES

namespace Project.TestModule.Aggregates.AggregateName1
{
    public class Child
    {
        public Parent MyParent { get; set; } // aggregate
        public AgeClass Age { get; set; } //value object
    }
}

namespace Project.TestModule.Aggregates.AggregateName2
{
    public class Parent
    {
        public Child MyChild { get; set; }
    }
}

namespace Project.TestModule.ValueObjects
{
    public class AgeClass
    {
        public DateTime BirthDay { get; set; }
        public TimeSpan Years { get; set; }
    }
}


// NOT DDD PROPERTIES

namespace Project.TestModule.ValueObjects
{
    public class NotDDDPropsClass
    {
        public NotDDDClass Other { get; set; }

        public DateTime BirthDay { get; set; }

        private readonly List<string> _degrees = new();
        public IReadOnlyCollection<string> Degrees => _degrees;
    }
}

namespace contoso.irrealnamespace
{
    public class NotDDDClass { }
}


// GENERICS

namespace Project.TestModule.ValueObjects
{
    public class Generic1
    {
        public List<int> A { get; set; }
        public Dictionary<int,int> B { get; set; }
        public Generic2<int, int, int> D { get; set; }
        public List<List<int>> C { get; set; }
        public List<List<Dictionary<string,string>>> E { get; set; }
    }
}

namespace contoso.generics
{
    public class Generic2<T,U, V>
    {
    }
}
