﻿using Project.ModuleName.Aggregates.AggregateName;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;

namespace domain.generator.tests.TClasses
{
    public static class A_Config
    {
        // cache root
        private static CompilationUnitSyntax _rootA1;
        public static CompilationUnitSyntax A1 => _rootA1 ??= Helper.GetRoot("A1_Classes");

        // classes
        public static List<ClassDeclarationSyntax> A1ClassSyntax { get; set; } = A1
            .DescendantNodes()
            .OfType<ClassDeclarationSyntax>()
            .ToList();

#pragma warning disable CA2211 // Non-constant fields should not be visible
        public static ClassDeclarationSyntax AClassSyntax0 = A1ClassSyntax.First();
        public static ClassDeclarationSyntax AClassSyntax1 = A1ClassSyntax[1];
        public static ClassDeclarationSyntax AClassSyntax2 = A1ClassSyntax[2];
#pragma warning restore CA2211 // Non-constant fields should not be visible

        #region dictionaries
        public static Dictionary<string, Dictionary<string, List<ClassDeclarationSyntax>>> NewEmptyDict() => new();
        #endregion
    }

}
