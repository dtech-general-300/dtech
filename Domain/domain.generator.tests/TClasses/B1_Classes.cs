﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#pragma warning disable IDE0051 // Remove unused private members
namespace Project.ModuleName.Aggregates.AggregateName
{
    public class B_DomainClassAggregate : Entity
    {
        public string fieldA;

        protected string fieldB;

        private const int A = 0;
        private readonly int fieldC = 0;

        public string PropA { get; set; }
        protected string PropB { get; set; }
        public string PropC { get; set; }
        private string PropD { get; set; }

        public B_DomainClassAggregate()
        {
            PropD = fieldC.ToString();
            PropC = PropD;
        }

    }

    public class Entity : ValueObject
    {
        public int Id { get; set; }
        private int Count { get; set; }
    }

    public class ValueObject
    {
        public int Quantity { get; set; }
    }
#pragma warning restore IDE0051 // Remove unused private members
}
