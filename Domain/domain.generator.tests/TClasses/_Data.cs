﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace domain.generator.tests.TClasses
{
    public static class Data
    {
        public static Dictionary<string, string> Map { get; set; } = new()
        {
            { "Aggregates", "En" },
            { "ValueObjects", "Vo" }
        };

        public const string GeneratorFiles_NamespaceFormat = "domain.generator.";

        public static string FormattedName(string subFolderName, string name) => $"{Map[subFolderName]}{name}Dto";

        // https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types
        public static readonly HashSet<string> builtInTypes = new()
        {
            "bool",
            "byte",
            "sbyte",
            "char",
            "decimal",
            "double",
            "float",
            "int",
            "uint",
            "long",
            "ulong",
            "short",
            "ushort",
            "object",
            "string",
            "dynamic"
        };

        public const string relatedPath = @"..\..\..\TClasses\"; // because all is in bin debug folder
    }

    public class ValidDddNamespacesAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            yield return new object[] { "domain.ModuleName.Aggregates.AggregateName" };
            yield return new object[] { "domain.ModuleName.ValueObjects" };
        }
    }

    public class BadLengthNamespacesAttribute : DataAttribute
    {
        public override IEnumerable<object[]> GetData(MethodInfo testMethod)
        {
            yield return new object[] { "domain" };
            yield return new object[] { "domain.ModuleName" };
        }
    }
}
