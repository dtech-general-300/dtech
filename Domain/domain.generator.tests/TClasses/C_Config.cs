﻿using domain.generator.B_Info;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit.Sdk;

namespace domain.generator.tests.TClasses
{
    #region Attributes C2
    public class DddPropsDataAttribute : DataAttribute
    {
        public static readonly List<BTypeInfo> types = C_Config.GetPropsFromClass(0);
        private static readonly List<object[]> data = new()
        {
            new object[] { types[0], Data.FormattedName("Aggregates", "Parent") },
            new object[] { types[1], Data.FormattedName("ValueObjects", "AgeClass") }
        };
        public override IEnumerable<object[]> GetData(MethodInfo testMethod) => data;
    }

    public class NotDddPropsDataAttribute : DataAttribute
    {
        public static readonly List<BTypeInfo> types = C_Config.GetPropsFromClass(3);
        private static readonly List<object[]> data = new()
        {
            new object[] { types[0], "using contoso.irrealnamespace;" },
            new object[] { types[1], "using System;" },
            new object[] { types[2], "using System.Collections.Generic;" }
        };
        public override IEnumerable<object[]> GetData(MethodInfo testMethod) => data;
    }

    public class GenericPropsDataAttribute : DataAttribute
    {
        public static readonly List<object[]> types = C_Config.GetPropsFromClass(5).Select(p => new object[] { p }).ToList();
        public override IEnumerable<object[]> GetData(MethodInfo testMethod) => types;
    }
    #endregion

    #region Configuration
    public static class C_Config
    {
        #region C2
        private static CompilationUnitSyntax _rootC2;
        public static CompilationUnitSyntax C2 => _rootC2 ??= Helper.GetRoot("C2_Classes");

        public static List<ClassDeclarationSyntax> C2ClassesSyntax { get; set; } = C2
                .DescendantNodes()
                .OfType<ClassDeclarationSyntax>()
                .ToList();
        private static SyntaxTree TreeC2 { get; set; } = C2ClassesSyntax.First().SyntaxTree;

        public static List<BTypeInfo> GetPropsFromClass(int index)
        {
            var classSyntax = C2ClassesSyntax[index];
            var classSymbol = SemanticC2.GetDeclaredSymbol(classSyntax);

            return classSymbol.GetMembers()
                .Where(symbol =>
                    symbol.Kind == SymbolKind.Property &&
                    symbol is IPropertySymbol { DeclaredAccessibility: Accessibility.Public })
                .Cast<IPropertySymbol>()
                .Select(p => new BTypeInfo(p.Type))
                .ToList();
        }
        #endregion


        #region Compilation
        public static CSharpCompilation Compilation { get; set; } = Helper.GetCompilation(new SyntaxTree[] { TreeC2 });
        public static SemanticModel SemanticC2 { get; set; } = Compilation.GetSemanticModel(TreeC2);
        #endregion
    }
    #endregion
}
