﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.ModuleName.Aggregates.AggregateName
{
    public class B2_BuiltInProperties
    {
        public bool P01 { get; set; }
        public byte P02 { get; set; }
        public sbyte P03 { get; set; }
        public char P04 { get; set; }
        public decimal P05 { get; set; }
        public double P06 { get; set; }
        public float P07 { get; set; }
        public int P08 { get; set; }
        public uint P09 { get; set; }
        public long P10 { get; set; }
        public ulong P11 { get; set; }
        public short P12 { get; set; }
        public ushort P13 { get; set; }
        public object P14 { get; set; }
        public string P15 { get; set; }
        public dynamic P16 { get; set; }
    }

    public class B2_MoreProperties
    {
        private readonly List<string> _degrees = new();
        public IReadOnlyCollection<string> Degrees => _degrees;

        public DateTime BirthDate { get; init; }

        private TimeSpan? _value;
        public TimeSpan Value => _value ??= TimeSpan.FromSeconds(10);
    }
}
