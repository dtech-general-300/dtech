﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace domain.generator.tests.TClasses
{
    // for cache (improve performace) and other things
    public static class Helper
    {
        #region Compilation
        
        public static CSharpCompilation GetCompilation(SyntaxTree[] trees) => CSharpCompilation.Create("foo",
                trees,
                References,
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

        public static List<MetadataReference> References { get; set; } = GetReferences();

        public static List<MetadataReference> GetReferences()
        {
            var references = new List<MetadataReference>();
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                if (!assembly.IsDynamic && !string.IsNullOrWhiteSpace(assembly.Location))
                {
                    references.Add(MetadataReference.CreateFromFile(assembly.Location));
                }
            }
            return references;
        }
        #endregion



        #region Get Files

        public static string FilePath(string fileName, string related = Data.relatedPath)
            => Path.Combine(Environment.CurrentDirectory, related, fileName);

        public static CompilationUnitSyntax GetRoot(string fileName, string extension = ".cs")
        {
            string code = GetCode(fileName, extension);
            return CSharpSyntaxTree.ParseText(code).GetCompilationUnitRoot();
        }

        private static string GetCode(string fileName, string extension)
        {
            if (!fileName.Contains("."))
                fileName += extension; // añadir extension solo si no la tiene

            var path = FilePath(fileName);
            if (!File.Exists(path))
                throw new ArgumentException("El archivo no existe.");

            var code = File.ReadAllText(path);
            return code;
        }
        #endregion

    }
}
