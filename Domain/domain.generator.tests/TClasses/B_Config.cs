﻿using domain.generator.B_Info;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit.Sdk;

namespace domain.generator.tests.TClasses
{
    #region Attributes B2
    public class BuiltInPropsDataAttribute : DataAttribute
    {
        private static readonly List<object[]> builtInProps = B_Config.GetProps(0).Select(p => new object[] { p }).ToList();
        public override IEnumerable<object[]> GetData(MethodInfo testMethod) => builtInProps;
    }

    public class MorePropsDataAttribute : DataAttribute
    {
        private static readonly List<object[]> moreProps = B_Config.GetProps(1).Select(p => new object[] { p }).ToList();
        public override IEnumerable<object[]> GetData(MethodInfo testMethod) => moreProps;
    }
    #endregion

    #region Configuration
    public static class B_Config
    {
        #region B1

        private static CompilationUnitSyntax _rootB1;
        public static CompilationUnitSyntax B1 => _rootB1 ??= Helper.GetRoot("B1_Classes");

        public static List<ClassDeclarationSyntax> B1ClassesSyntax { get; set; } = B1
                .DescendantNodes()
                .OfType<ClassDeclarationSyntax>()
                .ToList();

        private static SyntaxTree TreeB1 { get; set; } = B1ClassesSyntax.First().SyntaxTree;
        #endregion

        #region B2
        private static CompilationUnitSyntax _rootB2;
        public static CompilationUnitSyntax B2 => _rootB2 ??= Helper.GetRoot("B2_Classes");

        public static List<ClassDeclarationSyntax> B2ClassesSyntax { get; set; } = B2
                .DescendantNodes()
                .OfType<ClassDeclarationSyntax>()
                .ToList();

        private static SyntaxTree TreeB2 { get; set; } = B2ClassesSyntax.First().SyntaxTree;
        public static List<BTypeInfo> GetProps(int index)
        {
            var classSyntax = B2ClassesSyntax[index];
            var classSymbol = SemanticB2.GetDeclaredSymbol(classSyntax);

            return classSymbol.GetMembers()
                .Where(symbol =>
                    symbol.Kind == SymbolKind.Property &&
                    symbol is IPropertySymbol { DeclaredAccessibility: Accessibility.Public })
                .Cast<IPropertySymbol>()
                .Select(p => new BTypeInfo(p.Type))
                .ToList();
        }
        #endregion


        public static CSharpCompilation Compilation { get; set; } = Helper.GetCompilation(new SyntaxTree[] {
            TreeB1,
            TreeB2
        });
        public static SemanticModel SemanticB1 { get; set; } = Compilation.GetSemanticModel(TreeB1);
        public static SemanticModel SemanticB2 { get; set; } = Compilation.GetSemanticModel(TreeB2);

    }
    #endregion
}
