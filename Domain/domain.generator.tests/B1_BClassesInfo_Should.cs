﻿using FluentAssertions;
using domain.generator.B_Info;
using domain.generator.tests.TClasses;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Linq;
using Xunit;

namespace domain.generator.tests
{
    public class B1_BClassesInfo_Should
    {
        public static ClassDeclarationSyntax ClassSyntax { get; set; } = B_Config.B1ClassesSyntax.First();
        public static BClassInfo ClassInfo { get; set; } = new(B_Config.SemanticB1.GetDeclaredSymbol(ClassSyntax), ClassSyntax);

        [Fact]
        public void OnlyAdd_Public_Props()
        {
            var props = ClassInfo.Props.Select(p => p.Modifier);

            props.Should().AllBeEquivalentTo(Accessibility.Public);
        }

        [Fact]
        public void Add_Public_InheritedProps_From_Entity_BaseClass()
        {
            var propsString = ClassInfo.Props.Select(p => p.Name);

            propsString.Should().Contain("Id");
        }

        [Fact]
        public void NotAdd_Any_InheritedProps_FromA_BaseClass_Different_than_Entity()
        {
            var propsString = ClassInfo.Props.Select(p => p.Name);

            propsString.Should().NotContain("Quantity");
        }
    }
}
