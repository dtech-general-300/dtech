﻿using FluentAssertions;
using domain.generator.Helpers;
using domain.generator.tests.TClasses;
using System.Text;
using Xunit;

namespace domain.generator.tests
{
    public class C4_CreateClass_Should
    {
        private const string content = "// nothing";
        private const string className = "MyClass";
        private const string subFolderName = "Aggregates";
        public static string Indent(byte Tab) => new(' ', 4 * Tab);


        [Fact]
        public void Return_A_Class_With_Indent_Of_1_Tabs()
        {
            var result = Code.CreateClass(subFolderName, className, content);

            result.Should().StartWith(Indent(1));
        }

        [Fact]
        public void Return_A_PublicPartialClass()
        {
            var result = Code.CreateClass(subFolderName, className, content);

            result.Trim().Should().StartWith("public partial class");
        }

        [Fact]
        public void Return_The_Correct_DtoClassNameFormat()
        {
            var sb = new StringBuilder();
            sb.AppendLine($" {Data.FormattedName(subFolderName,className)}");

            var result = Code.CreateClass(subFolderName, className, content);

            result.Should().Contain(sb.ToString());
        }

    }
}
