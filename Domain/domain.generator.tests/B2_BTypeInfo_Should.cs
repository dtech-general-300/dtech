﻿using FluentAssertions;
using domain.generator.B_Info;
using domain.generator.tests.TClasses;
using Xunit;

namespace domain.generator.tests
{
    public class B2_BTypeInfo_Should
    {

        [Theory]
        [BuiltInPropsData]
        public void Have_IsBuiltInProperty_True_For_builtInTypes(BTypeInfo type)
        {
            type.IsBuiltInType.Should().BeTrue();
        }

        [Theory]
        [BuiltInPropsData]
        [MorePropsData]
        public void NotHave_FullName_WithPoints_When_IsBuiltInProperty_IsTrue(BTypeInfo type)
        {
            var result = type.IsBuiltInType ^ type.FullName.Contains('.');

            result.Should().BeTrue();
        }

        [Theory]
        [BuiltInPropsData]
        [MorePropsData]
        public void NotHave_Name_Property_WithPoints(BTypeInfo type)
        {
            var result = !type.Name.Contains('.');

            result.Should().BeTrue();
        }

        [Theory]
        [BuiltInPropsData]
        [MorePropsData]
        public void Have_GenericArgumentList_Populated_If_IsGeneric_IsTrue(BTypeInfo type)
        {
            if (type.IsGeneric)
                type.GenericArguments.Should().NotBeEmpty();
            else
                type.GenericArguments.Should().BeEmpty();

        }

    }
}
