using FluentAssertions;
using domain.generator.A_Syntax;
using domain.generator.tests.TClasses;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace domain.generator.tests
{
    public class A1_AddDomainClassesNodesExt_Should
    {
        #region Empty Dict
        [Fact]
        public void AddClassNode_To_EmptyDictDictList()
        {
            // Arrange
            var dict = A_Config.NewEmptyDict();

            // Act
            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            var listItem = dict.Values.FirstOrDefault()?.Values.FirstOrDefault()?.FirstOrDefault();

            // Assert
            dict.Should().HaveCount(1);
            listItem.Should().Be(A_Config.AClassSyntax0);
        }

        [Fact]
        public void AddClassNode_To_EmptyDictDictList_WithCorrect_FirstDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            var firstDict = dict.FirstOrDefault();

            firstDict.Key.Should().Be(nameof(Project.ModuleName)); // nameof only takes last namespace
        }

        [Fact]
        public void AddClassNode_To_EmptyDictDictList_WithCorrect_SecondDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            var secondDict = dict.Values.FirstOrDefault().First();

            secondDict.Key.Should().Be(nameof(Project.ModuleName.Aggregates)); // nameof only takes last namespace
        }
        #endregion

        #region Populated Dict

        [Fact]
        public void Allow_ToAddClassNode_To_DictDictList_WithAn_Existing_FirstDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            dict.AddDomainClassesNodes(A_Config.AClassSyntax0); // add node with the same ModuleName

            dict.Keys.Should().HaveCount(1);
        }

        [Fact]
        public void Allow_ToAddClassNode_To_DictDictList_WithAn_Existing_SecondDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            dict.AddDomainClassesNodes(A_Config.AClassSyntax0); // add node with the same SubFolderName
            var secondDict = dict.Values.First();

            secondDict.Keys.Should().HaveCount(1);
        }

        [Fact]
        public void AddClassNode_To_DictDictList_Even_WithExisting_FirstAndSecondDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            dict.AddDomainClassesNodes(A_Config.AClassSyntax0); // add node with the same SubFolderName
            var list = dict.Values.FirstOrDefault()?.Values.FirstOrDefault();

            list.Should().HaveCount(2);
        }

        [Fact]
        public void Allow_ToAddClassNode_To_DictDictList_WithA_New_FirstDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            dict.AddDomainClassesNodes(A_Config.AClassSyntax2); // add node with a new ModuleName

            dict.Keys.Should().HaveCount(2);
        }

        [Fact]
        public void Allow_ToAddClassNode_To_DictDictList_WithA_ExistingFirstDictKey_And_New_SecondDictKey()
        {
            var dict = A_Config.NewEmptyDict();

            dict.AddDomainClassesNodes(A_Config.AClassSyntax0);
            dict.AddDomainClassesNodes(A_Config.AClassSyntax1); // add node with the same ModuleName but a new SubFolderName
            var secondDict = dict.Values.First();

            secondDict.Keys.Should().HaveCount(2);
        }


        #endregion


    }
}

