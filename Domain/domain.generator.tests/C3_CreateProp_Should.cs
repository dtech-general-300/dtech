﻿using FluentAssertions;
using domain.generator.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace domain.generator.tests
{
    public class C3_CreateProp_Should
    {
        private const string type = "int";
        private const string propName = "MyProp";
        public static string Indent(byte Tab) => new(' ', 4 * Tab);


        [Fact]
        public void Return_A_PropWith_Indent_Of_2_Tabs()
        {
            var result = Code.CreateProp(type, propName);

            result.Should().StartWith(Indent(2));
        }

        [Fact]
        public void Return_A_PropWith_Public_KeyWord()
        {
            var result = Code.CreateProp(type, propName);

            result.Trim().Should().StartWith("public ");
        }

        [Fact]
        public void Return_Type_After_Public_KeyWord()
        {
            var result = Code.CreateProp(type, propName);

            result.Should().Contain($"public {type}");
        }

        [Fact]
        public void Return_Name_After_The_Type()
        {
            var result = Code.CreateProp(type, propName);

            result.Should().Contain($"{type} {propName}");
        }

        [Fact]
        public void Return_An_Autoproperty()
        {
            var result = Code.CreateProp(type, propName);

            result.Should().EndWith("{ get; set; }");
        }
    }
}
