﻿using System;

#pragma warning disable IDE0041 // Use 'is null' check
namespace domain.core.SeedWork
{
    public abstract class Entity
    {
        public int Id { get; set; }

        public static bool operator ==(Entity a, Entity b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Entity a, Entity b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            var other = obj as Entity;

            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != obj.GetType())
                return false;

            if (Id == 0 || other.Id == 0)
                return false;

            return Id == other.Id;
        }

        public override int GetHashCode() // if after compare HashCodes, result is true, then Equals get called
        {
            return (GetType().ToString() + Id).GetHashCode();
        }

    }
}

#pragma warning restore IDE0041 // Use 'is null' check