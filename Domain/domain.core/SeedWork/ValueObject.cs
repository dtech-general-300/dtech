﻿using System.Collections.Generic;
using System.Linq;

#pragma warning disable IDE0041 // Use 'is null' check
namespace domain.core.SeedWork
{
    public abstract class ValueObject<T>
        where T : ValueObject<T>
    {

        // OPERATORS

        public static bool operator ==(ValueObject<T> a, ValueObject<T> b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(ValueObject<T> a, ValueObject<T> b)
        {
            return !(a == b);
        }


        // EQUALS

        public override bool Equals(object obj)
        {
            var other = obj as T;

            if (ReferenceEquals(other, null))
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != obj.GetType())
                return false;

            return EqualsCore(other);
        }

        protected virtual bool EqualsCore(T other) // method separated in case override needed
        {
            return GetEqualityComponents().SequenceEqual(other.GetEqualityComponents());
        }

        protected abstract IEnumerable<object> GetEqualityComponents();


        // GET HASH CODE

        public override int GetHashCode() // if after compare HashCodes, result is true, then Equals get called
        {
            return GetHashCodeCore();
        }

        protected int GetHashCodeCore() // method separated in case override needed
        {
            return GetEqualityComponents()
                    .Select(x => x != null ? x.GetHashCode() : 0)
                    .Aggregate((x, y) => x ^ y);
        }



    }
}

#pragma warning restore IDE0041 // Use 'is null' check