﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

#pragma warning disable IDE0041 // Use 'is null' check
namespace domain.core.SeedWork
{
    public abstract class Enumeration : IComparable
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        protected Enumeration(int id, string name) => (Id, Name) = (id, name);

        public override string ToString() => Name;

        public int CompareTo(object other) => Id.CompareTo(((Enumeration)other).Id);

        public static IEnumerable<T> GetAll<T>() where T : Enumeration =>
            typeof(T).GetFields(BindingFlags.Public |
                                BindingFlags.Static |
                                BindingFlags.DeclaredOnly)
                     .Select(f => f.GetValue(null))
                     .Cast<T>();

        public override bool Equals(object obj)
        {
            if (obj is not Enumeration other) // si obj es null, return false; copia obj en other
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (GetType() != obj.GetType())
                return false;

            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }


        // OPERATORS

        public static bool operator ==(Enumeration a, Enumeration b)
        {
            if (ReferenceEquals(a, null) && ReferenceEquals(b, null))
                return true;

            if (ReferenceEquals(a, null) || ReferenceEquals(b, null))
                return false;

            return a.Equals(b);
        }

        public static bool operator !=(Enumeration a, Enumeration b)
        {
            return !(a == b);
        }
    }
}
#pragma warning restore IDE0041 // Use 'is null' check
