﻿using domain.core.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace domain.core.SeedWork
{
    public interface IDRepository<T> where T : Entity, IAggregateRoot
    {
        void Add(T entity);
        //void Delete(object id);
        //void Delete(T entityToDelete);
        void Delete(T entity);
        IEnumerable<T> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "");
        T GetById(int id);
        T GetByID(object id);
        Task<T> GetByIdAsync(int id);
        Task<bool> Exists(int id);
        IEnumerable<T> GetWithRawSql(string query, params object[] parameters);
        void Insert(T entity);
        Task<List<T>> ListAsync();
        void Update(T entityToUpdate);
        //Task UpdateAsync(T entity);
        //Task GetByIdAsync(int? id);

        Task Commit();
        void SetAsModified(T record);
    }
}
