﻿using domain.generator.B_Info;
using System.Collections.Generic;
using System.Text;

namespace domain.generator.Helpers
{
    public class Usings // for test
    {
        public HashSet<string> Inserted { get; set; } = new();
        public StringBuilder Header { get; set; } = new();
        public string HeaderString => Inserted.Count == 0 ? Header.ToString() : Header.AppendLine().ToString();

        public void AddUsing(string nameSpace)
        {
            if (!Inserted.Contains(nameSpace))
            {
                Header.AppendLine($"using {nameSpace};");
                Inserted.Add(nameSpace);
            }
        }
    }

    public static class Code
    {
        public static string Indent(byte Tab) => new(' ', 4 * Tab);

        #region DtoProperties
        public static string ToDtoProperty(this BPropertyInfo Prop, string moduleName, Usings usings) 
            => CreateProp(Prop.Type.ToString(moduleName, usings), Prop.Name);

        public static string CreateProp(string type, string name) // for test
            => $"{Indent(2)}public {type} {name} {{ get; set; }}";
        
        public static string ToString(this BTypeInfo type, string moduleName, Usings usings) // for test
        {
            var str = type.Name;
            if (!type.IsBuiltInType)
                if (Folders.IsDddNamespace(type.Namespace, out var module_name, out var subFolderName))
                {
                    str = $"{Folders.Map[subFolderName]}{str}Dto";
                    if (module_name != moduleName) // if not belong to same Module
                        usings.AddUsing($"{Folders.baseNamespace}.{moduleName}");
                }
                else
                {
                    if (str == "IReadOnlyCollection")
                        str = "List"; // to allow editing collections (we dont change Namespace, cause it is the same)

                    usings.AddUsing(type.Namespace);
                }

            if (type.IsGeneric)
            {
                List<string> args = new();
                foreach (var argProp in type.GenericArguments)
                    args.Add(argProp.ToString(moduleName, usings));
                str += $"<{string.Join(", ", args.ToArray())}>";
            }
            return str;
        }

        #endregion


        #region DtoClasses
        public static string ToDtoClass(this BClassInfo Class, string moduleName, Usings usings, string subFolderName)
        {
            var props = new StringBuilder();
            foreach (var Prop in Class.Props)
                props.AppendLine(Prop.ToDtoProperty(moduleName, usings));
            return CreateClass(subFolderName, Class.Name, props.ToString());
        }

        public static string CreateClass(string subFolderName, string name, string content) // for test
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{Indent(1)}public partial class {Folders.Map[subFolderName]}{name}Dto");
            sb.AppendLine($"{Indent(1)}{{");
            sb.Append(content);
            sb.AppendLine($"{Indent(1)}}}");
            return sb.ToString();
        }
        #endregion


        #region DtoNamespaces
        public static string ConvertToDtoFile(string moduleName, Dictionary<string, IEnumerable<BClassInfo>> moduleClasses)
        {
            var usings = new Usings();
            var classes = new StringBuilder();

            foreach (var folder in moduleClasses)
                foreach (var domainClass in folder.Value)
                    classes.AppendLine(domainClass.ToDtoClass(moduleName, usings, folder.Key));

            return usings.HeaderString + CreateNamespace(moduleName, classes.ToString());
        }

        public static string CreateNamespace(string moduleName, string content) // for test
        {
            var sb = new StringBuilder();
            sb.AppendLine($"namespace {Folders.baseNamespace}.{moduleName}");
            sb.AppendLine("{");
            sb.Append(content);
            sb.AppendLine("}");
            return sb.ToString();
        }
        #endregion
    }
}
