using System.Collections.Generic;
using System.Linq;

namespace domain.generator.Helpers
{
    public static class Folders
    {

        #region Config

        public const string baseNamespace = "domain.generator";

        // Actual Project: domain.moduleFolder.subFolders
        // subFolders=(Aggregates, ValueObjects, etc.)
        public const byte moduleIndex = 1;
        public const byte subFolderIndex = 2;
        public const byte MinNamespaceLength = 3; // because namespace at least must have n items


        // Mapping Folder Names with DDD classes types
        public static Dictionary<string, string> Map { get; set; } = new()
        {
            { "Aggregates", "En" },
            { "ValueObjects", "Vo" }
        };

        #endregion

        #region Helper Methods

        /// <summary> 
        /// Check if the given namespace is with a DDD structure, so it has a moduleName and subfolder.
        /// </summary>
        /// <returns>FALSE: can't get folder names </returns>
        /// <param name="nameSpace">"namespaceA.namespaceB.namespaceC" format</param>
        public static bool IsDddNamespace(string nameSpace, out string moduleName, out string subFolderName)
        {
            moduleName = "";
            subFolderName = "";

            // split namespace
            var namespaceArray = nameSpace.Split('.');
            if (namespaceArray.Length < MinNamespaceLength)
                return false;

            // get names
            moduleName = namespaceArray[moduleIndex];
            subFolderName = namespaceArray[subFolderIndex];

            // check if namespace has a ddd folder
            if (!Map.ContainsKey(subFolderName))
                return false;

            return true;
        }

        #endregion

    }
}