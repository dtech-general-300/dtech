﻿using System;
using System.Collections.Generic;
using System.Text;

namespace domain.generator.Helpers
{
    public static class DictEasyAddExtension
    {
        public static void SafeAdd<TKey,TValue>( this IDictionary<TKey, TValue> dict, 
            TKey key, 
            TValue valueIfNewKey,
            Action<TValue> actionIfKeyExist)
        {
            if (dict.ContainsKey(key))
                actionIfKeyExist(dict[key]);
            else
                dict.Add(key, valueIfNewKey);
        }
    }
}
