using Microsoft.CodeAnalysis;
using domain.generator.A_Syntax;
using System.Collections.Generic;

namespace domain.generator.B_Info
{
    /// <summary>
    /// //////////////// 2ST ////////////////////
    /// This is the second step, before generate.
    /// I get Info for the gathered syntax Nodes
    /// </summary>
    public partial class NodesInfo
    {
        private readonly GeneratorExecutionContext _context;
        private readonly SyntaxReceiver _syntaxReceiver;
        
        ///////

        public Dictionary<string, Dictionary<string, IEnumerable<BClassInfo>>> DomainModules { get; }

        public NodesInfo(GeneratorExecutionContext context, SyntaxReceiver syntaxReceiver)
        {
            _context = context;
            _syntaxReceiver = syntaxReceiver;

            // call info methods
            DomainModules = GetClassesInfo();
        }

    }
}