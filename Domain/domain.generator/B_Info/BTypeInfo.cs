using System.Collections.Generic;
using Microsoft.CodeAnalysis;

namespace domain.generator.B_Info
{
    public class BTypeInfo
    {
        public bool IsNamedType { get; set; } = false; // because are others like IArrayTypeSymbol
        public bool IsBuiltInType { get; set; } = false; // c# built-in types: string, int, etc. 
        public bool IsGeneric { get; set; } = false;

        public string FullName { get; set; } // "namespaceA.namespaceB.namespaceC.MyType"
        public string Namespace { get; set; } // "namespaceA.namespaceB.namespaceC"
        public string Name { get; set; } // "MyType"
        public List<BTypeInfo> GenericArguments { get; set; } = new();

        public BTypeInfo(ITypeSymbol typeSymbol)
        {
            FullName = typeSymbol.ToString(); // only has namespace if it is not built-in type
            Namespace = typeSymbol.ContainingNamespace?.ToString() ?? "";
            IsBuiltInType = builtInTypes.Contains(FullName);
            Name = IsBuiltInType ? FullName : typeSymbol.Name;

            if (typeSymbol is INamedTypeSymbol namedType)
            {
                IsNamedType = true;
                IsGeneric = namedType.IsGenericType;
                if (IsGeneric)
                    foreach (var arg in namedType.TypeArguments)
                        GenericArguments.Add(new BTypeInfo(arg));
            }
        }

        #region Built-in types

        // go to https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/built-in-types
        public static HashSet<string> builtInTypes = new()
        {
            "bool",
            "byte",
            "sbyte",
            "char",
            "decimal",
            "double",
            "float",
            "int",
            "uint",
            "long",
            "ulong",
            "short",
            "ushort",
            "object",
            "string",
            "dynamic"
        };

        #endregion

    }
}