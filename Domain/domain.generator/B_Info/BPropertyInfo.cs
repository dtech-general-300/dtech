using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace domain.generator.B_Info
{
    public class BPropertyInfo
    {
        public Accessibility Modifier { get; set; }
        public BTypeInfo Type { get; set; }
        public string Name { get; set; }

        public BPropertyInfo(IPropertySymbol property)
        {
            Modifier = property.DeclaredAccessibility;
            Name = property.Name;
            Type = new BTypeInfo(property.Type);
        }
    }
}