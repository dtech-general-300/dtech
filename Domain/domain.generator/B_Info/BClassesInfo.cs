using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;

namespace domain.generator.B_Info
{
    public class BClassInfo
    {
        public INamedTypeSymbol ClassSymbol { get; }
        public ClassDeclarationSyntax ClassNode { get; }

        ///////

        public string Name { get; }
        public List<BPropertyInfo> Props { get; } = new();

        public BClassInfo(INamedTypeSymbol type, ClassDeclarationSyntax node)
        {
            ClassSymbol = type;
            ClassNode = node;
            Name = ClassNode.Identifier.ValueText; // DomainClassType.Name
            GetProps(ClassSymbol);
        }

        private void GetProps(INamedTypeSymbol classSymbol)
        {
            // Inherited Props
            if (classSymbol.BaseType is INamedTypeSymbol { Name: "Entity" } baseType)
                GetProps(baseType);
            
            // Own Props
            foreach (var symbol in classSymbol.GetMembers().Where(s => s.Kind == SymbolKind.Property))
                if (symbol is IPropertySymbol { DeclaredAccessibility: Accessibility.Public } property)
                    Props.Add(new BPropertyInfo(property));

        }
    }


    // Convetions: node -> info

    public partial class NodesInfo
    {
        public Dictionary<string, Dictionary<string, IEnumerable<BClassInfo>>> GetClassesInfo()
        {
            var dict = new Dictionary<string, Dictionary<string, IEnumerable<BClassInfo>>>();
            foreach (var module in _syntaxReceiver.CandidateClasses)
            {
                var subDict = new Dictionary<string, IEnumerable<BClassInfo>>();
                foreach (var folder in module.Value)
                    subDict.Add(folder.Key, DomainClassNodeToInfo(folder.Value));
                dict.Add(module.Key, subDict);
            }
            return dict;
        }

        private IEnumerable<BClassInfo> DomainClassNodeToInfo(List<ClassDeclarationSyntax> list)
        {
            foreach (var classNode in list)
            {
                var semanticModel = _context.Compilation.GetSemanticModel(classNode.SyntaxTree);
                yield return new BClassInfo(semanticModel.GetDeclaredSymbol(classNode), classNode);
            }
        }

    }
}