﻿using domain.generator.A_Syntax;
using domain.generator.B_Info;
using Microsoft.CodeAnalysis;
using System;
using System.Diagnostics;

namespace domain.generator.C_Source
{
    [Generator]
    public class SourceGenerator : ISourceGenerator
    {
        public void Initialize(GeneratorInitializationContext context)
        {
            context.RegisterForSyntaxNotifications(() => new SyntaxReceiver());

//             Debug.WriteLine("Initalize code generator");
// #if DEBUG
//             if (!Debugger.IsAttached)
//             {
//                 Debugger.Launch();
//             }
// #endif 

        }

        public void Execute(GeneratorExecutionContext context)
        {
            if (context.SyntaxReceiver is not SyntaxReceiver syntaxReceiver)
                throw new InvalidOperationException("Invalid syntax receiver");

            var info = new NodesInfo(context, syntaxReceiver);

            context.AddDomainClasses(info);
        }

    }
}
