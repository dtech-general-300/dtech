using domain.generator.B_Info;
using Microsoft.CodeAnalysis;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using domain.generator.Helpers;
using System;

namespace domain.generator.C_Source
{
    public static class AddDomainClassesExt
    {
        public static void AddDomainClasses(this GeneratorExecutionContext context, NodesInfo info)
        {
            foreach (var module in info.DomainModules)
            {
                var comments = new StringBuilder();
                comments.AppendLine("// auto generated");
                comments.AppendLine($"// AssemblyName: {context.Compilation.AssemblyName}");
                comments.AppendLine("");

                var code = comments.ToString() + Code.ConvertToDtoFile(module.Key, module.Value);
                context.AddSource($"{module.Key}.cs", code);
            }

        }

    }
}