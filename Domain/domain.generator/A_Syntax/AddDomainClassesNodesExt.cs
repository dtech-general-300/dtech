﻿using domain.generator.Helpers;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;

namespace domain.generator.A_Syntax
{
    public static class AddDomainClassesNodesExt
    {
        public static void AddDomainClassesNodes(
            this Dictionary<string, Dictionary<string, List<ClassDeclarationSyntax>>> classes,
            SyntaxNode syntaxNode)
        {

            if (syntaxNode is ClassDeclarationSyntax classSyntax // solo clases
                && classSyntax.Parent is NamespaceDeclarationSyntax namespaceSyntax // dentro de un namespace
            )
            {
                // get names
                if (!Folders.IsDddNamespace(namespaceSyntax.Name.ToString(), out var moduleName, out var subFolderName))
                    return;

                // set up dicts
                var list = new List<ClassDeclarationSyntax> { classSyntax };
                var dict = new Dictionary<string, List<ClassDeclarationSyntax>> { { subFolderName, list } };

                classes.SafeAdd(
                    key: moduleName,
                    valueIfNewKey: dict, 
                    actionIfKeyExist: d => d.SafeAdd(
                        subFolderName, 
                        list, 
                        ll => ll.Add(classSyntax)
                    )
                );
            }

        }
    }

}
