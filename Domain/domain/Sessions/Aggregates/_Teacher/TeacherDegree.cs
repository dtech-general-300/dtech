﻿using domain.core.SeedWork;
using domain.Sessions.Aggregates._Degree;
using System;

namespace domain.Sessions.Aggregates._Teacher
{
    public sealed class TeacherDegree : Entity
    {
        public Teacher Teacher { get; private set; }
        public Degree Degree { get; private set; }
        public int TotalGrade { get; private set; }

        private TeacherDegree() { }
        public TeacherDegree(Degree degree, int totalGrade)
        {
            Degree = degree;
            SetTotalGrade(totalGrade);
        }

        public void SetTotalGrade(int totalGrade)
        {
            if (totalGrade < 7)
                throw new ArgumentException("At least Total Grade should be 7");
            TotalGrade = totalGrade;
        }
    }
}
