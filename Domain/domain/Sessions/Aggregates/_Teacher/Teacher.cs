﻿using domain.core.SeedWork;
using domain.Sessions.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace domain.Sessions.Aggregates._Teacher
{
    public sealed class Teacher : Entity, IAggregateRoot
    {
        public SimpleName Name { get; private set; }
        public PersonAge Age { get; private set; }

        private readonly List<TeacherDegree> _degrees = new();
        public IReadOnlyCollection<TeacherDegree> Degrees => _degrees;

        private Teacher() { }
        public Teacher(SimpleName name, PersonAge age)
        { 
            Update(name, age);
        }

        public void Update(SimpleName name, PersonAge age)
        {
            Name = name;
            Age = age;
        }

        public void AddDegree(TeacherDegree degree)
        {
            var searchedDegree = _degrees.Where(x => x == degree).SingleOrDefault();
            bool isNewDegree = searchedDegree == null;

            if (isNewDegree)
                _degrees.Add(degree);
            else
                throw new ArgumentException("Already exists!");
        }

    }
}
