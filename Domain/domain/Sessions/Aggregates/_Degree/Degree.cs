﻿using domain.core.SeedWork;

namespace domain.Sessions.Aggregates._Degree
{
    public sealed class Degree : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }

        public Degree(string name)
        {
            Name = name.ToUpper();
        }

        public void SetDescription(string description)
        {
            Description = description;
        }


    }
}
