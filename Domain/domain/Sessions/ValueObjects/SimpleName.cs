﻿using domain.core.SeedWork;
using System;
using System.Collections.Generic;

namespace domain.Sessions.ValueObjects
{
    public class SimpleName : ValueObject<SimpleName>
    {
        public string Value { get; init; }

        private SimpleName(string value)
        {
            Value = value;
        }

        public static Result<SimpleName> Create(string name)
        {
            name = (name ?? string.Empty).Trim();

            if (name.Length == 0)
                return Result.Fail<SimpleName>("The name should not be empty");

            if (name.Length > 100)
                return Result.Fail<SimpleName>("The name is too long");

            return Result.Ok(new SimpleName(name));
        }

        #region core
        public static implicit operator string(SimpleName name)
        {
            return name.Value;
        }

        public static explicit operator SimpleName(string name)
        {
            return Create(name).Value;
        }

        protected override bool EqualsCore(SimpleName other)
        {
            return Value.Equals(other.Value, StringComparison.InvariantCultureIgnoreCase);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }
        #endregion
    }
}
