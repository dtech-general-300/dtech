﻿using domain.core.SeedWork;
using System;
using System.Collections.Generic;

namespace domain.Sessions.ValueObjects
{
    public class PersonAge : ValueObject<PersonAge>
    {
        public DateTime BirthDate { get; init; }

        private TimeSpan? _value;
        public TimeSpan Value => _value ??= DateTime.Now.Subtract(BirthDate);
        
        private int? _years;
        public int Years => _years ??= (int)Math.Floor(Value.TotalDays / 365.25);

        private PersonAge(DateTime birthDate)
        {
            BirthDate = birthDate;
        }

        public static Result<PersonAge> Create(DateTime birthDate)
        {
            if (birthDate > DateTime.Now)
                return Result.Fail<PersonAge>("Birth Date can't be greater than current Date");

            return Result.Ok(new PersonAge(birthDate));
        }

        #region core
        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return BirthDate;
        }
        #endregion
    }
}
