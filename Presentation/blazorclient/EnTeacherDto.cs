﻿using System;
using System.Collections.Generic;

namespace blazorclient
{
    public partial class VoPersonAgeDto
    {
        public DateTime BirthDate { get; set; }
        public TimeSpan Value { get; set; }
        public int Years { get; set; }
    }

    public partial class VoSimpleNameDto
    {
        public string Value { get; set; }
    }

    public partial class EnDegreeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public partial class EnTeacherDegreeDto
    {
        public int Id { get; set; }
        public EnTeacherDto Teacher { get; set; }
        public EnDegreeDto Degree { get; set; }
        public int TotalGrade { get; set; }
    }

    public partial class EnTeacherDto
    {
        public int Id { get; set; }
        public VoSimpleNameDto Name { get; set; }
        public VoPersonAgeDto Age { get; set; }
        public List<EnTeacherDegreeDto> Degrees { get; set; }
    }
}
