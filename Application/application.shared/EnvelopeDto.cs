﻿using System;

namespace application.shared._SeedWork
{
    public class EnvelopeDto<T>
    {
        public T Result { get; init; }
        public string ErrorMessage { get; init; }
        public DateTime TimeGenerated { get; init; }
    }
}
