﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace application.shared
{
    /// <summary>
    /// This class contain the info of a "field created or changed by the user" in a record
    /// For Simple fields: use NewValue
    /// For ToMany fields: use ToManyActions
    /// </summary>
    public class Value
    {
        public string FieldName { get; }
        public object NewValue { get; }

        public bool IsToMany { get; } = false;
        public List<ToManyActions> ToManyActions { get; set; } = new();

        public Value(string fieldName, object newValue)
        {
            FieldName = fieldName;
            NewValue = newValue;
        }

        public Value(string fieldName)
        {
            FieldName = fieldName;
            IsToMany = true;
        }
    }

    public class ToManyActions
    {
        public ActionType Type { get; set; }
        public int Id { get; set; }
        public List<Value> Values { get; set; } = new();
        public List<int> Ids { get; set; } = new();
    }

    public enum ActionType
    {
        AddNewRecord,               // usa Values
        UpdateExistingRecord,       // usa Id y Values
        RemoveRecordFromSetAndDB,   // usa Id
        RemoveRecordFromSet,        // usa Id
        AddExistingRecordToSet,     // usa Id
        RemoveAllRecordsFromSet,    // no usa nada
        ReplaceAllRecordsFromSet    // usa Ids
    }

}
