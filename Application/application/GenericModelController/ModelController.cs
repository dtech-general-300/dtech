﻿//using application._SeedWork;
//using domain._SeedWork;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Linq.Expressions;
//using System.Threading.Tasks;


//namespace application.GenericModelController
//{
//    [Authorize]
//    [ApiController]
//    [Route("generic")] // this route will ve override by RouteConvention
//    public class ModelController<T> : BaseController<T> where T : Entity, IAggregateRoot
//    {
//        public ModelController(IDRepository<T> repo) : base(repo) { }

//        [HttpGet]
//        public async Task<IActionResult> GetAll()
//        {
//            return Ok(await _repo.ListAsync());
//        }

//        [HttpGet("{id}")]
//        public async Task<IActionResult> GetById(int? id)
//        {
//            if (id is null) return NotFound();
//            var record = await _repo.GetByIdAsync((int)id);
//            return record == null ? NotFound() : Ok(record);
//        }

//        //// PUT
//        //// To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//        //[HttpPut("update")]
//        ////[ValidateAntiForgeryToken]
//        //public async Task<IActionResult> Update(T jsonData)
//        //{
//        //    Type U = typeof(TeacherDto).Assembly.GetType("dtech.SharedApplication.Dtos." + typeof(T).Name + "Dto");
//        //    var recordDto = Convert.ChangeType(jsonData, U);
//        //    IMapper mapper = new Mapper();
//        //    T record = mapper.Map<T>(recordDto);
//        //    _repo.SetAsModified(record);

//        //    try
//        //    {
//        //        return await SaveAndOk();
//        //    }
//        //    catch (DbUpdateConcurrencyException)
//        //    {
//        //        if (!await _repo.Exists(record.Id))
//        //            return NotFound();
//        //        else
//        //            return Error("Concureency error!");
//        //    }
//        //}

//        // POST
//        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
//        [HttpPost("create")]
//        public async Task<IActionResult> Post(T record)
//        {
//            _repo.Add(record);
//            return await SaveAndOk(record);
//        }


//        // DELETE
//        [HttpDelete("{id}")]
//        public async Task<IActionResult> Delete(int id)
//        {

//            //var a0c = typeof(T).GetConstructor(Array.Empty<Type>());
//            Func<T> creator = Expression.Lambda<Func<T>>(Expression.New(typeof(T))).Compile();
//            T record = creator();
//            record.Id = id;
//            _repo.Delete(record);
//            return await SaveAndOk();
//        }

//    }

//}
