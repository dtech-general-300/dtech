﻿//using domain._SeedWork;
//using dtech.SharedDomain;
//using Microsoft.AspNetCore.Mvc.ApplicationParts;
//using Microsoft.AspNetCore.Mvc.Controllers;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;

//namespace application.GenericModelController
//{
//    public class ModelFeatureProvider : IApplicationFeatureProvider<ControllerFeature>
//    {
//        public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
//        {
//            var currentAssembly = typeof(Entity).Assembly;
//            var candidates = currentAssembly.GetExportedTypes()
//                .Where(x => x.GetCustomAttributes<ModelControllerAttribute>().Any());

//            foreach (var candidate in candidates)
//            {
//                feature.Controllers.Add(
//                    typeof(ModelController<>).MakeGenericType(candidate).GetTypeInfo()
//                );
//            }
//        }
//    }
//}
