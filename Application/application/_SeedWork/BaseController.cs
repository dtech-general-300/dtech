﻿using domain.core.SeedWork;
using application.shared._SeedWork;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace application._SeedWork
{
    public class BaseController<T> : ControllerBase where T: Entity, IAggregateRoot
    {
        protected readonly IDRepository<T> _repo;
        public BaseController(IDRepository<T> repo) => _repo = repo;
        

        // For Querys

        /// <summary>
        /// For querys, no commit
        /// </summary>
        /// <returns>An Ok response, status 200</returns>
        protected new IActionResult Ok() => base.Ok(Envelope.Ok());

        /// <summary>
        /// For querys, no commit
        /// </summary>
        /// <returns>An Ok response, status 200, with the result</returns>
        protected IActionResult Ok<U>(U result) => base.Ok(Envelope.Ok(result));



        // For Commands

        /// <summary>
        /// For Commands, commit and return ok
        /// </summary>
        /// <returns>An Ok response, status 200</returns>
        protected async Task<IActionResult> SaveAndOk()
        {
            await _repo.Commit();
            return Ok();
        }

        /// <summary>
        /// For Commands, commit and return ok
        /// </summary>
        /// <returns>An Ok response, status 200, with the result</returns>
        protected async Task<IActionResult> SaveAndOk<U>(U result)
        {
            await _repo.Commit();
            return Ok(result);
        }



        // For Bad Request

        /// <summary>
        /// Indica la razón de un error al devolver un badrequest
        /// </summary>
        /// <returns>A bad request</returns>
        protected IActionResult Error(string errorMessage)
        {
            return BadRequest(Envelope.Error(errorMessage));
        }
    }
}
