﻿using domain.core.SeedWork;
using domain.Sessions.Aggregates._Teacher;
using domain.generator.Sessions;
using Mapster;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace application
{
    public class GetAllTeacherQuery : IRequest<List<EnTeacherDto>>
    {
    }

    public class GetAllTeacherQueryHandler : IRequestHandler<GetAllTeacherQuery, List<EnTeacherDto>>
    {
        protected readonly IDRepository<Teacher> _repo;

        public GetAllTeacherQueryHandler(IDRepository<Teacher> repo)
        {
            _repo = repo;
        }

        public async Task<List<EnTeacherDto>> Handle(GetAllTeacherQuery request, CancellationToken cancellationToken)
        {
            var teachers = await _repo.ListAsync();
            List<EnTeacherDto> dtos = teachers.Select(x => x.Adapt<EnTeacherDto>()).ToList();
            return dtos;
        }
    }
}
