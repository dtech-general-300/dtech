﻿
using application._SeedWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using domain.generator.Sessions;
using domain.Sessions.Aggregates._Teacher;
using domain.core.SeedWork;
using domain.Sessions.ValueObjects;

namespace application
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TeacherBehaviourController : BaseController<Teacher>
    {
        public TeacherBehaviourController(IDRepository<Teacher> repo) : base(repo) { }

        [HttpPost]
        [Route("onchange_name")]
        public IActionResult OnChange_Name(EnTeacherDto teacherDto)
        {
            // create value
            var newAge = PersonAge.Create(teacherDto.Age.BirthDate.AddYears(-teacherDto.Name.Value.Length));
            if (newAge.IsFailure)
                return Error(newAge.Error);

            // map & save
            teacherDto.Age = new()
            {
                BirthDate = newAge.Value.BirthDate,
                Value = newAge.Value.Value,
                Years = newAge.Value.Years
            };

            return Ok(teacherDto);
        }

    }
}
