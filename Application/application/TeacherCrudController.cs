﻿
using application._SeedWork;
using domain.core.SeedWork;
using domain.generator.Sessions;
using domain.Sessions.Aggregates._Teacher;
using domain.Sessions.ValueObjects;
using Mapster;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace application
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class TeacherCrudController : BaseController<Teacher>
    {
        private readonly IMediator _mediator;

        public TeacherCrudController(IDRepository<Teacher> repo, IMediator mediator) : base(repo)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetList()
        {
            var query = new GetAllTeacherQuery();
            var teachers = await _mediator.Send(query);
            return Ok(teachers);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetById(int? id)
        {
            if (id is null) return NotFound();
            var teacher = await _repo.GetByIdAsync((int)id);
            if (teacher == null) return NotFound();

            return Ok(teacher.Adapt<EnTeacherDto>()); // it returns DTO
        }


        // POST: Teachers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Route("create")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EnTeacherDto teacherDto)
        {
            // validation
            var resName = SimpleName.Create(teacherDto.Name.Value);
            var resAge = PersonAge.Create(teacherDto.Age.BirthDate);
            Result result = Result.Combine(resName, resAge);
            if (result.IsFailure)
                return Error(result.Error);

            // map & save
            var teacher = new Teacher(resName.Value, resAge.Value);
            _repo.Add(teacher);
            return await SaveAndOk(teacher.Adapt<EnTeacherDto>());
        }


        [HttpPut]
        [Route("update")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(EnTeacherDto teacherDto)
        {
            // validation
            var resName = SimpleName.Create(teacherDto.Name.Value);
            var resAge = PersonAge.Create(teacherDto.Age.BirthDate);
            Result result = Result.Combine(resName, resAge);
            if (result.IsFailure)
                return Error(result.Error);

            var teacher = await _repo.GetByIdAsync(teacherDto.Id);
            if (teacher == null)
                return Error("Invalid teacher id: " + teacherDto.Id);


            try
            {
                // map & save
                teacher.Update(resName.Value, resAge.Value);
                _repo.Update(teacher);
                return await SaveAndOk();
            }
            catch (DbUpdateConcurrencyException)
            {
                return Error("Concureency error!");
            }
        }

        //[HttpPut]
        //[Route("changes")]
        ////[ValidateAntiForgeryToken]
        //public async Task<IActionResult> UpdateChanges(List<Value> teacherDto)
        //{
        //    // validation
        //    var resName = SimpleName.Create(teacherDto.Name.Value);
        //    var resAge = PersonAge.Create(teacherDto.Age.BirthDate);
        //    Result result = Result.Combine(resName, resAge);
        //    if (result.IsFailure)
        //        return Error(result.Error);

        //    var teacher = await _repo.GetByIdAsync(teacherDto.Id);
        //    if (teacher == null)
        //        return Error("Invalid teacher id: " + teacherDto.Id);


        //    try
        //    {
        //        // map & save
        //        teacher.Update(resName.Value, resAge.Value);
        //        _repo.Update(teacher);
        //        return await SaveAndOk();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        return Error("Concureency error!");
        //    }
        //}


        [HttpDelete("{id}")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            var teacher = new Teacher(
                SimpleName.Create("empty").Value,
                PersonAge.Create(new DateTime()).Value
            )
            { Id = id };
            _repo.Delete(teacher);
            return await SaveAndOk(id);
        }

    }
}
