﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Globalization;

namespace server.Resources.SwitchCulture
{
    public class SwitchCultureModel
    {
        public CultureInfo CurrentUICulture { get; set; }
        public List<SelectListItem> SupportedCultures { get; set; }
    }
}
