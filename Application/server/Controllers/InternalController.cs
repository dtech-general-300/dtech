﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

// This controller is made for Identity pages localization

namespace server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InternalController : ControllerBase
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SwitchCulture([FromForm] string culture, string currentUrl)
        {
            Response.Cookies.Append(
                ".AspNetCore.Custom.Culture",
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            return LocalRedirect(currentUrl);
        }
    }
}
