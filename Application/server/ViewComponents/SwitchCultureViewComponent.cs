﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace server.Resources.SwitchCulture
{
    public class SwitchCultureViewComponent : ViewComponent
    {
        private readonly IOptions<RequestLocalizationOptions> localizationOptions;
        
        public SwitchCultureViewComponent(IOptions<RequestLocalizationOptions> locOpt)
        {
            localizationOptions = locOpt;
        }
        
        public IViewComponentResult Invoke()
        {
            var model = new SwitchCultureModel
            {
                CurrentUICulture = HttpContext
                    .Features
                    .Get<IRequestCultureFeature>()
                    .RequestCulture
                    .UICulture,
                SupportedCultures = localizationOptions
                    .Value
                    .SupportedUICultures
                    .Select(c => new SelectListItem { Value = c.Name, Text = c.EnglishName })
                    .ToList()
            };
            return View(model);
        }


    }
}
