﻿using domain.core.SeedWork;
using persistence._Persistence;
using Microsoft.Extensions.DependencyInjection;

namespace crosscutting.IoC
{
    public static class ServiceRegistrationExtentions
    {
        public static void AddCrossCuttingServices(this IServiceCollection services)
        {
            services.AddDbContext<DContext>();
            services.AddTransient(typeof(IDRepository<>), typeof(DRepository<>));
        }
    }
}
