﻿using domain.Sessions.Aggregates._Degree;
using domain.Sessions.Aggregates._Teacher;
using Microsoft.EntityFrameworkCore;

namespace persistence._Persistence
{
    public sealed class DContext : DbContext
    {
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Degree> Degrees { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(@"Server=localhost;Port=5432;Database=dbtest;User Id=manzanita;Password=manzanita;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
            => modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);

    }
}
