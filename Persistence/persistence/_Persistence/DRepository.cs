﻿using domain.core.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Linq.Expressions;


namespace persistence._Persistence
{
    /// <summary>
    /// Generic Repository
    /// </summary>
    public class DRepository<T> : IDRepository<T> where T : Entity, IAggregateRoot
    {
        private readonly DContext _context;
        private readonly DbSet<T> _entity;

        public DRepository(DContext dbContext)
        {
            _context = dbContext;
            _entity = _context.Set<T>();
        }


        public async Task<List<T>> ListAsync() // este estaba sin async await
        {
            return await _entity.ToListAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await _entity.SingleOrDefaultAsync(e => e.Id == id);
        }

        public async Task<bool> Exists(int id)
        {
            return await _entity.AnyAsync(o => o.Id == id);
        }

        public void Add(T entity)
        {
            _entity.Add(entity);
        }



        public T GetById(int id)
        {
            return _entity.SingleOrDefault(e => e.Id == id);
        }

        //public Task Update(T entity)
        //{
        //    _context.Entry(entity).State = EntityState.Modified;
        //    return _context.SaveChangesAsync();
        //}

        public void Delete(T entity)
        {
            _entity.Remove(entity);
        }



        // 

        public IEnumerable<T> GetWithRawSql(string query, params object[] parameters)
        {
            return _entity.FromSqlRaw(query, parameters);
        }

        public IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<T> query = _entity;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (includeProperties != null)
            {
                foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProperty);
                }
            }


            if (orderBy != null)
            {
                return orderBy(query).ToList();
            }
            else
            {
                return query.ToList();
            }
        }

        public T GetByID(object id)
        {
            return _entity.Find(id);
        }

        public void Insert(T entity)
        {
            _entity.Add(entity);
        }

        //public void Delete(object id)
        //{
        //    T entityToDelete = _entity.Find(id);
        //    Delete(entityToDelete);
        //}

        //public void Delete(T entityToDelete)
        //{
        //    if (_context.Entry(entityToDelete).State == EntityState.Detached)
        //    {
        //        _entity.Attach(entityToDelete);
        //    }
        //    _entity.Remove(entityToDelete);
        //}

        public void Update(T entityToUpdate)
        {
            //_entity.det
            _entity.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }

        public void SetAsModified(T record)
        {
            _context.Entry(record).State = EntityState.Modified;
        }
    }
}
