﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using domain.Sessions.Aggregates._Teacher;
using domain.Sessions.Aggregates._Degree;

namespace persistence.Sessions._TeacherMap
{
    public class TeacherDegreeMap : IEntityTypeConfiguration<TeacherDegree>
    {
        public void Configure(EntityTypeBuilder<TeacherDegree> builder)
        {
            builder.MiddleTableConfig(
                PrincipalNavPropName: nameof(TeacherDegree.Teacher), 
                SecondaryNavPropName: nameof(TeacherDegree.Degree));
        }
    }
}
