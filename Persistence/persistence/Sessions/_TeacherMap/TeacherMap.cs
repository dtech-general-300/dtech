﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using domain.Sessions.Aggregates._Teacher;
using persistence.Sessions.ValueObjectMap;

namespace persistence.Sessions._TeacherMap
{
    public class TeacherMap : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.ValueObject(b => b.Name);
            builder.ValueObject(b => b.Age);
        }
    }
}
