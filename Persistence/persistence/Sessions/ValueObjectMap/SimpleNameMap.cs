﻿using domain.core.SeedWork;
using domain.Sessions.ValueObjects;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Linq.Expressions;

namespace persistence.Sessions.ValueObjectMap
{
    public static partial class ValueObjectBuilder
    {
        public static EntityTypeBuilder<T> ValueObject<T>(
            this EntityTypeBuilder<T> builder,
            Expression<Func<T, SimpleName>> navigationExpression)
        where T : Entity => builder.OwnsOne(navigationExpression, c =>
        {
            c.Property(x => x.Value).IsRequired();
        });
    }
}
