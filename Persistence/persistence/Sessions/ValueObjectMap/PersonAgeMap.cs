﻿using domain.core.SeedWork;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.EntityFrameworkCore.Utilities;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using domain.Sessions.ValueObjects;
using domain.Sessions.Aggregates._Teacher;
using domain.Sessions.Aggregates._Degree;

namespace persistence.Sessions.ValueObjectMap
{
    public static partial class ValueObjectBuilder
    {
        public static EntityTypeBuilder<T> ValueObject<T>(
            this EntityTypeBuilder<T> builder,
            Expression<Func<T, PersonAge>> navigationExpression)
        where T : Entity => builder.OwnsOne(navigationExpression, c =>
        {
            c.Property(x => x.BirthDate).HasComment("Age computed based on BirthDate");
        });
    }
}
