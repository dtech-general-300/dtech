﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using domain.Sessions.Aggregates._Degree;
using domain.Sessions.ValueObjects;

namespace persistence.Sessions._DegreeMap
{
    public class DegreeMap : IEntityTypeConfiguration<Degree>
    {
        public void Configure(EntityTypeBuilder<Degree> builder)
        {
            builder.Property(b => b.Name).IsRequired();
        }
    }
}
